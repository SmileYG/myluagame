EventType = {}

EventType.ENTER_SCENE = "enter_scene"
EventType.EXITE_SCENE = "exite_scene"
EventType.RESET_SCENE = "reset_scene"

EventType.CREATE_GRID = "create_grid"
--EventType.TOUCH_BEGAN_GRID = "touch_began_grid"
EventType.TOUCH_EVENT = "touch_event"

EventType.SET_DOING_ID = "set_doing_id"
EventType.UNSET_DOING_ID = "unset_doing_id"

EventType.DO_SKILL = "do_skill"
EventType.DO_HIT = "do_hit"
EventType.SKILL_END = "skill_end"
EventType.NORMAL_ATTACK_EVENT = "normal_attack_event"
EventType.ATTACK_HIT = "attack_hit"
EventType.HIT_END = "hit_end"
EventType.SKILL_ATTACK_EVENT = "skill_attack_event"
EventType.SKILL_MULT_EVENT = "skill_mult_event"
EventType.SKILL_ACTION_END = "skill_action_end"
EventType.ADD_SKILL_EFFECT = "add_skill_effect"

EventType.SKILL_EXECUTE_EVENT = "skill_execute_event"
EventType.EFFECT_FRAME_EVENT = "effect_frame_event"

EventType.ATTACK_PROCESS_EVENT = "attack_process_event"

EventType.BUFER_WORK = "buffer_work"
