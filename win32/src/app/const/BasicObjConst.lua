local BasicObjConst = {}

BasicObjConst.TYPE_CHAR = "char"
BasicObjConst.TYPE_PROP = "prop"

BasicObjConst.SCALE_TIME = 0.1
BasicObjConst.SCALE_TO_LARGE = 1
BasicObjConst.SCALE_TO_ORIGINAL = 0.8 
BasicObjConst.ITEM_SCALE_TO_LARGE = 1.3

BasicObjConst.ITEM_GOBACK_TIME = 0.1

BasicObjConst.ROTATE_INTERVAL = 0.1
BasicObjConst.ROTATE_DEGREE = 5

BasicObjConst.MOVE_TIME = 0.5

BasicObjConst.DIR_LEFT = -1
BasicObjConst.DIR_RIGHT = 1

BasicObjConst.POINT_STAND = "standPoint"
BasicObjConst.POINT_SIGN = "signPoint"
BasicObjConst.POINT_HIT = "hitPoint"
BasicObjConst.POINT_ATTACK = "attackPoint"
BasicObjConst.POINT_HP = "hpPoint"

BasicObjConst.SCALE_STATUS_LARGE = "large"
BasicObjConst.SCALE_STATUS_ORIGINAL = "original"

return BasicObjConst
