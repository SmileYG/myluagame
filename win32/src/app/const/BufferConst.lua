local BufferConst = {}

BufferConst.PROP_DAMAGE = "PROP_DAMAGE"
BufferConst.PROP_HP = "PROP_HP"
BufferConst.PROP_CP = "PROP_CP"
BufferConst.PROP_STR = "PROP_STR"
BufferConst.PROP_ATS = "PROP_ATS"
BufferConst.PROP_DEF = "PROP_DEF"
BufferConst.PROP_ADF = "PROP_ADF"
BufferConst.PROP_MOV = "PROP_MOV"

BufferConst.TYPE_ITEM = 1 
BufferConst.TYPE_ATTACK = 2 

return BufferConst
