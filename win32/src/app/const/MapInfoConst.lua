local MapInfoConst = {}

MapInfoConst.FORCE_NULL = 0

-- 场上所有对象
MapInfoConst.OCCUPY_ALL = 100

-- 己方
MapInfoConst.FORCE_OWN = 10

-- 敌方
MapInfoConst.FORCE_ENEMY = 20

-- 友方
--MapInfoConst.FORCE_ALLIANCE = 30


MapInfoConst.GRID_HOR = 9
MapInfoConst.GRID_VER = 5

MapInfoConst.CAN_MOVE = 0
MapInfoConst.CAN_NOT_MOVE = 50

MapInfoConst.HOR_SCALE_Y = {0.75, 0.84, 0.87, 0.89, 0.98}
MapInfoConst.COLUMN_SKEW_X = {5, 4, 3, 1.5, 0, -1.5, -3, -4, -5}

MapInfoConst.POS_Y = {502, 415, 326, 234, 133}

MapInfoConst.X_INFO = {
    {{x = 79, scaleX = 0.96}, {x = 179, scaleX = 0.9}, {x = 273, scaleX = 0.89}, {x = 367, scaleX = 0.92}, {x = 464, scaleX = 0.9}, {x = 558, scaleX = 0.92}, {x = 655, scaleX = 0.89}, {x = 749, scaleX = 0.9}, {x = 843, scaleX = 0.91}}, 
    {{x = 70, scaleX = 0.98}, {x = 173, scaleX = 0.9}, {x = 268, scaleX = 0.91}, {x = 364, scaleX = 0.93}, {x = 462, scaleX = 0.92}, {x = 559, scaleX = 0.93}, {x = 658, scaleX = 0.92}, {x = 755, scaleX = 0.91}, {x = 852, scaleX = 0.92}}, 
    {{x = 61, scaleX = 0.98}, {x = 164, scaleX = 0.94}, {x = 262, scaleX = 0.93}, {x = 361, scaleX = 0.95}, {x = 461, scaleX = 0.94}, {x = 561, scaleX = 0.95}, {x = 661, scaleX = 0.94}, {x = 761, scaleX = 0.94}, {x = 861, scaleX = 0.93}}, 
    {{x = 52, scaleX = 0.99}, {x = 155, scaleX = 0.98}, {x = 258, scaleX = 0.94}, {x = 357, scaleX = 0.97}, {x = 459, scaleX = 0.97}, {x = 561, scaleX = 0.97}, {x = 664, scaleX = 0.97}, {x = 767, scaleX = 0.96}, {x = 868, scaleX = 0.96}}, 
    {{x = 43, scaleX = 0.99}, {x = 148, scaleX = 0.98}, {x = 253, scaleX = 0.95}, {x = 354, scaleX = 0.98}, {x = 458, scaleX = 0.99}, {x = 563, scaleX = 0.99}, {x = 668, scaleX = 1}, {x = 772, scaleX = 0.99}, {x = 876, scaleX = 0.99}}, 
}

return MapInfoConst
