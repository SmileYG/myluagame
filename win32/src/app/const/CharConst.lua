local CharConst = {}

CharConst.PROP_HP = "HP"
CharConst.PROP_CP = "CP"
CharConst.PROP_STR = "STR"
CharConst.PROP_ATS = "ATS"
CharConst.PROP_DEF = "DEF"
CharConst.PROP_ADF = "ADF"
CharConst.PROP_MOV = "MOV"

CharConst.ACTION_STAND = "stand"
CharConst.ACTION_ATTACK = "attack"
CharConst.ACTION_MOVE = "move"
CharConst.ACTION_SKILL = "skill"
CharConst.ACTION_DEATH = "death"
CharConst.ACTION_HIT = "hit"

CharConst.ATTACK_MODE_NORMAL = "normal_attack"
CharConst.ATTACK_MODE_SKILL = "skill_attack"

CharConst.ATTACK_FRAME_EVENT = "attackFrameEvent"
CharConst.SKILL_FRAME_EVENT = "skillFrameEvent"
CharConst.SKILL_MULT_EVENT = "skillMultEvent"

--[[
-- 被阻挡
CharConst.ATTACK_TYPE_BLOCKED = "blocked"

-- 不被阻挡
CharConst.ATTACK_TYPE_NON_BLOCKED = "non_blocked"

-- 穿刺
CharConst.ATTACK_TYPE_PUNCTURE = "puncture"
]]--


-- 能被攻击
CharConst.CAN_BE_ATTACKED = "can_be_attacked"
-- 不能被攻击
CharConst.CAN_NOT_BE_SELECTED = "can_not_be_selected"
-- 可选中
CharConst.WAITING_SELECT = "waiting_select"
-- 被救助
CharConst.CAN_BE_RESCUED = "can_be_rescued"


CharConst.HERO_TYPE_DOCTOR = "doctor"

return CharConst
