local BufferInfo = {
    ["buffer_damage_1"] = {
        ["propType"] = "PROP_DAMAGE", 
        ["rounds"] = 0,
        ["bufferType"] = 2, -- 1是道具buffer 2是攻击buffer
        --["host"] = 2, -- 1是自己  2是别人
        --["vo"] = 2, -- 1是挂载的对象  2是被挂载的对象
        ["effectId"] = "de_1003", -- DelayEffectInfo.lua
        ["mountPointName"] = "hitPoint",
    },
    ["buffer_damage_2"] = {
        ["propType"] = "PROP_DAMAGE", 
        --["isPercent"] = 0,
        ["rounds"] = 0,
        ["bufferType"] = 2, -- 1是道具buffer 2是攻击buffer
        --["host"] = 2, -- 1是自己  2是别人
        --["vo"] = 2, -- 1是挂载的对象  2是被挂载的对象
        ["effectId"] = "de_1010", -- DelayEffectInfo.lua
        ["mountPointName"] = "standPoint",
    },
    ["buffer_2"] = {
        ["propType"] = "PROP_HP", 
        --["isPercent"] = 0,
        ["rounds"] = 0,
        ["value"] = 90,
        ["bufferType"] = 1, -- 1是道具buffer 2是攻击buffer
        --["host"] = 2, -- 1是自己  2是别人
        --["vo"] = 2, -- 1是挂载的对象  2是被挂载的对象
        ["effectId"] = "de_1006",
        ["mountPointName"] = "hitPoint",
    },
    ["buffer_3"] = {
        ["propType"] = "PROP_STR", 
        --["isPercent"] = 1,
        ["rounds"] = 5,
        ["value"] = 100,
        ["bufferType"] = 1, -- 1是道具buffer 2是攻击buffer
        --["host"] = 2, -- 1是自己  2是别人
        --["vo"] = 2, -- 1是挂载的对象  2是被挂载的对象
        ["effectId"] = "de_1004",
        ["mountPointName"] = "hitPoint",
    },
    ["buffer_4"] = {
        ["propType"] = "PROP_DEF", 
        --["isPercent"] = 1,
        ["rounds"] = 5,
        ["value"] = -50,
        ["bufferType"] = 1, -- 1是道具buffer 2是攻击buffer
        --["host"] = 2, -- 1是自己  2是别人
        --["vo"] = 2, -- 1是挂载的对象  2是被挂载的对象
        ["effectId"] = "de_1005",
        ["mountPointName"] = "hitPoint",
    },
    ["buffer_5"] = {
        ["propType"] = "PROP_CP", 
        --["isPercent"] = 0,
        ["rounds"] = 0,
        ["value"] = 100,
        ["bufferType"] = 1, -- 1是道具buffer 2是攻击buffer
        --["host"] = 2, -- 1是自己  2是别人
        --["vo"] = 2, -- 1是挂载的对象  2是被挂载的对象
        ["effectId"] = "de_1004",
        ["mountPointName"] = "hitPoint",
    },
}

return BufferInfo
