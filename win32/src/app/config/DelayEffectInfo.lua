local DelayEffectInfo = {
    -----技能特效和buffer特效最好区分开------------
    ["de_1001"] = {
        ["framesNum"] = 24,
        ["framesDelayTime"] = {
            [1] =  0.09,
            [2] =  0.12,
            [24] =  0.15, 
        },
        ["plistName"] = "0021.plist",
        ["executeFrame"] = 11,
        --["parent"] = 0, -- 0:特效层上 1:攻击者 2:被攻击者  
    },
    ["de_1002"] = {
        ["framesNum"] = 6,
        ["framesDelayTime"] = {
            [1] =  0.09,
        },
        ["plistName"] = "0028.plist",
        --["parent"] = 1, -- 0:特效层上 1:攻击者 2:被攻击者  
    },
    ["de_1003"] = {
        ["framesNum"] = 4,
        ["framesDelayTime"] = {
            [1] =  0.09,
        },
        ["plistName"] = "0003.plist",
        --["parent"] = 2, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "hitPoint"
    },
    ["de_1004"] = {
        ["framesNum"] = 12,
        ["framesDelayTime"] = {
            [1] =  0.09,
        },
        ["plistName"] = "0025.plist",
        --["parent"] = 2, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "hitPoint"
    },
    ["de_1005"] = {
        ["framesNum"] = 10,
        ["framesDelayTime"] = {
            [1] =  0.12,
        },
        ["plistName"] = "0005.plist",
        --["parent"] = 2, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "hitPoint"
    },
    ["de_1006"] = {
        ["framesNum"] = 13,
        ["framesDelayTime"] = {
            [1] =  0.09,
        },
        ["plistName"] = "0000.plist",
        --["parent"] = 2, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "hitPoint"
    },
    ["de_1007"] = {
        ["framesNum"] = 4,
        ["executeFrame"] = 2,
        ["framesDelayTime"] = {
            [1] =  0.12,
            [2] =  0.15,
            [4] =  0.12, 
        },
        ["plistName"] = "0179.plist",
        --["parent"] = 0, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "G_standPoint"
    },
    ["de_1008"] = {
        ["framesNum"] = 16,
        ["executeFrame"] = 14,
        ["framesDelayTime"] = {
            [1] =  0.18,
            [2] =  0.09,
        },
        ["plistName"] = "0185.plist",
        --["parent"] = 2, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "hitPoint",
        ["anchorPoint"] = {0.5, 0},
    },
    ["projt_de_1009"] = {
        ["framesNum"] = 7,
        ["executeFrame"] = 0,
        ["framesDelayTime"] = {
            [1] =  0.07,
        },
        ["plistName"] = "0063.plist",
        --["parent"] = 0, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "hitPoint",
        ["anchorPoint"] = {0.5, 0},
        ["loop"] = 1,
    },
    ["de_1010"] = {
        ["framesNum"] = 6,
        ["executeFrame"] = 0,
        ["framesDelayTime"] = {
            [1] =  0.09,
        },
        ["plistName"] = "0096.plist",
        --["parent"] = 2, -- 0:特效层上 1:攻击者 2:被攻击者  
        --["mountPointName"] = "standPoint",
        ["anchorPoint"] = {0.5, 0},
    },
}

return DelayEffectInfo
