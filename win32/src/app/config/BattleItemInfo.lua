-- target: 
-- 100 所有人
-- 10:己方所有对象包括尸体，11:己方活着的对象，12:己方尸体
-- 20:敌方所有对象包括尸体，21:敌方活着的对象，22:敌方尸体
local BattleItemInfo = {
    ["item_1"] = {
        ["buffer"] ={
            "buffer_3",
        },
        ["res"] = "ui/battleUI/Sword-hd.png",
        ["targetType"] = 11
    },
    ["item_2"] = {
        ["buffer"] ={
            "buffer_2",
        },
        ["res"] = "ui/battleUI/Potion-hd.png",
        ["targetType"] = 11
    },
    ["item_3"] = {
        ["buffer"] ={
            "buffer_4",
        },
        ["res"] = "ui/battleUI/Shield-hd.png",
        ["targetType"] = 11
    },
    ["item_4"] = {
        ["buffer"] ={
            "buffer_5",
        },
        ["res"] = "ui/battleUI/Scroll-hd.png",
        ["targetType"] = 11
    },
    ["item_5"] = {
        ["buffer"] ={
            "buffer_3",
        },
        ["res"] = "ui/battleUI/Sword-hd.png",
        ["targetType"] = 11
    },
    ["item_6"] = {
        ["res"] = "ui/battleUI/Sword-hd.png",
    },
}

return BattleItemInfo
