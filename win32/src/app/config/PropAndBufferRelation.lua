local PropAndBufferRelation = {
    ["HP"] = {
        "TYPE_HP",
    },
    ["STR"] = {
        "TYPE_STR",
    },
}

return PropAndBufferRelation
