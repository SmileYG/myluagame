local NormalAttackInfo = {
    ["normalAttack10001"] = {
        ["effectId"] = "projt_1002",
        ["parent"] = 0,
        ["castPointName"] = "attackPoint",
        ["destPointName"] = "hitPoint",
        ["target_buffer"] = {"buffer_damage_1"},
    },
    ["normalAttack10002"] = {
        ["effectId"] = "projt_1003",
        ["parent"] = 0,
        ["castPointName"] = "attackPoint",
        ["destPointName"] = "hitPoint",
        ["target_buffer"] = {"buffer_damage_1"},
    },
    ["normalAttack10003"] = {
        ["target_buffer"] = {"buffer_damage_1"},
    },
    ["normalAttack10004"] = {
        ["effectId"] = "projt_1004",
        ["parent"] = 0,
        ["castPointName"] = "attackPoint",
        ["destPointName"] = "hitPoint",
        ["target_buffer"] = {"buffer_damage_1"},
    },
}

return NormalAttackInfo
