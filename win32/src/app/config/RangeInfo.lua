local RangeInfo = {
    ["rangeInfo_1"] = {
        {x = 0, y = 0},
    },

    -- o
    --oxo
    -- o
    ["rangeInfo_2"] = {
        {x = 0, y = 0}, 
        {x = -1, y = 0}, 
        {x = 0, y = 1}, 
        {x = 1, y = 0},
        {x = 0, y = -1},
    },

    --ooo
    --oxo
    --ooo
    ["rangeInfo_3"] = {
        {x = 0, y = 0}, 
        {x = -1, y = 0}, 
        {x = 0, y = 1}, 
        {x = 1, y = 0},
        {x = 0, y = -1},
        {x = -1, y = -1},
        {x = 1, y = 1},
        {x = -1, y = 1},
        {x = 1, y = -1},
    },

    --  o
    -- ooo
    --ooxoo
    -- ooo
    --  o
    ["rangeInfo_4"] = {
        {x = 0, y = 0}, 
        {x = -1, y = 0}, 
        {x = 0, y = 1}, 
        {x = 1, y = 0},
        {x = 0, y = -1},
        {x = -1, y = -1},
        {x = 1, y = 1},
        {x = -1, y = 1},
        {x = 1, y = -1},
        {x = 2, y = 0},
        {x = -2, y = 0},
        {x = 0, y = 2},
        {x = 0, y = -2},
    },

    --oxo
    ["rangeInfo_5"] = {
        {x = 0, y = 0},
        {x = -1, y = 0},
        {x = 1, y = 0},
    },

    --o   o
    ---o o
    ----x
    ---o o
    --o   o
    ["rangeInfo_6"] = {
        {x = 0, y = 0}, 
        {x = 1, y = 1}, 
        {x = 2, y = 2}, 
        {x = 1, y = -1},
        {x = 2, y = -2},
        {x = -1, y = -1},
        {x = -2, y = -2},
        {x = -1, y = 1},
        {x = -2, y = 2},
    },
}

return RangeInfo
