local SkillInfo = {
    ["skill10001"] = {
        ["skillId"] = "sp10001",
        ["effectId"] = "projt_de_1009",
        ["multiEffect"] = 1,
        ["fixedCastPoint"] = 1,
        ["mountParent"] = 0, -- 0:特效层上 1:攻击者 2:被攻击者  
        ["destPointName"] = "standPoint",
        ["target_buffer"] = {"buffer_damage_2"},
    },
    ["skill10002"] = {
        ["skillId"] = "sp10001",
        ["effectId"] = "de_1008",
        ["multiEffect"] = 1,
        ["mountParent"] = 2, -- 0:特效层上 1:攻击者 2:被攻击者  
        ["destPointName"] = "hitPoint",
        ["target_buffer"] = {"buffer_damage_1"},
    },
    ["skill10003"] = {
        ["skillId"] = "sp10001",
        ["self_buffer"] = {"buffer_2"},
        ["target_buffer"] = {"buffer_damage_1"},
    },
}

return SkillInfo
