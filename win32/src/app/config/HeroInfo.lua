local HeroInfo ={ 
    [10000] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2",
        ["attackType"] = 1, -- 被阻挡
        --["RNG"] = 2,
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "NewAnimation",
    },
    [10001] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_1", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "AM",
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = {
            --["projtId"] = xxx,
            --["impact_effectId"] = xxx,
            --["self_buffer"] = {xxx},
            ["target_buffer"] = {"buffer_damage_1"},
            --["isProjectile"] = 0,
        },
        -- 技能后加的buffer
        ["skill"] = {
            ["skillId"] = "skill1001",
            --["self_buffer"] = {xxx},
            ["target_buffer"] = {"buffer_damage_1"},
        },
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
    [10002] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 对应RangeInfo.lua
        ["n_range"] = "attackRange_1", -- 普攻攻击范围,对应AttackRange.lua
        ["s_range"] = "attackRange_1", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "Archer",
        ["HP"] = 300,
        ["CP"] = 0,
        ["STR"] = 550,
        ["DEF"] = 20,
        ["ADF"] = 10,
        ["attack"] = {
            ["effectId"] = "projt_1001",
            --["impact_effectId"] = xxx,
            --["self_buffer"] = {xxx},
            ["target_buffer"] = {"buffer_damage_1"},
            ["isProjectile"] = 1,
        },
    },
    [10003] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_3",
        ["attackType"] = 1, -- 被阻挡
        --["RNG"] = 1,
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "Coco",
        ["HP"] = 400,
        ["CP"] = 0,
        ["STR"] = 100,
        ["ATS"] = 100,
        ["DEF"] = 10,
        ["ADF"] = 20,
    },
    [10004] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2",
        ["attackType"] = 1, -- 被阻挡
        --["RNG"] = 1,
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "Bone",
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 250,
        ["ATS"] = 50,
        ["DEF"] = 0,
        ["ADF"] = 0,
    },
    [10005] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_1", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "ES",
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = {
            --["projtId"] = xxx,
            --["impact_effectId"] = xxx,
            --["self_buffer"] = {xxx},
            ["target_buffer"] = {"buffer_damage_1"},
            --["isProjectile"] = 0,
        },
        -- 技能后加的buffer
        ["skill"] = {
            ["skillId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        },
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
    [10006] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_2", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "JUGG", --剑圣
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = "normalAttack10003",
        -- 技能后加的buffer
        ["skill"] = "skill10003",
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
    [10007] = {
        ["MOV"] = 4,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_3", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "KOTL", --大法
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = "normalAttack10004",
        -- 技能后加的buffer
        ["skill"] = "skill10001",
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
    [10008] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_1", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "LOA", --死亡骑士
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = "normalAttack10003",
        -- 技能后加的buffer
        ["skill"] = {
            ["skillId"] = "skill10001",
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        },
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = "skill10001",
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
    [10009] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_1", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "Lich", --巫妖
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = {
            --["projtId"] = xxx,
            --["impact_effectId"] = xxx,
            --["self_buffer"] = {xxx},
            ["target_buffer"] = {"buffer_damage_1"},
            --["isProjectile"] = 0,
        },
        -- 技能后加的buffer
        ["skill"] = {
            ["skillId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        },
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
    [10010] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_1", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "Luna", --月骑
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = "normalAttack10002",
        -- 技能后加的buffer
        ["skill"] = {
            ["skillId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        },
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
     [10011] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_3", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "POM", --白虎妹妹
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 200, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = "normalAttack10001",
        -- 技能后加的buffer
        ["skill"] = "skill10002",
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
     [10012] = {
        ["MOV"] = 2,
        ["selectRange"] = 2, -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_1", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "TB",
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = {
            --["projtId"] = xxx,
            --["impact_effectId"] = xxx,
            --["self_buffer"] = {xxx},
            ["target_buffer"] = {"buffer_damage_1"},
            --["isProjectile"] = 0,
        },
        -- 技能后加的buffer
        ["skill"] = {
            ["skillId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        },
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
    [10013] = {
        ["MOV"] = 2,
        ["selectRange"] = "rangeInfo_2", -- 目标选择范围
        ["n_range"] = "attackRange_1", -- 普攻攻击范围
        ["s_range"] = "attackRange_2", -- 技能攻击范围
        ["attackType"] = 1, -- 被阻挡
        --["attackType"] = "non-puncture"  穿刺 
        --["profession"] = "doctor",
        ["helpOthers"] = 1,
        ["res"] = "Ursa", --拍拍熊
        ["HP"] = 500,
        ["CP"] = 0,
        ["STR"] = 400, -- 物攻
        --["ATS"] = 100, -- 魔攻
        ["DEF"] = 20,  -- 物防 是百分值
        ["ADF"] = 20,  -- 魔防 是百分值
        -- 普通攻击后加的buffer
        ["attack"] = "normalAttack10003",
        -- 技能后加的buffer
        ["skill"] = "skill10003",
        -- 治疗后加的buffer
        ["treat"] = {
            ["effectId"] = {xxx},
            ["self_buffer"] = {xxx},
            ["target_buffer"] = {xxx},
        }
    },
}

return HeroInfo
