--暂时只能以自身为中心,即center只能为0
--攻击范围配置只有一个坐标时，即range为rangeInfo_1，center应当为1
local AttackRange = {
    ["attackRange_1"] = {
        ["center"] = 0, -- 0:自身  1:选中的当前目标自身
        ["range"] = "rangeInfo_1", --对应RangeInfo.lua
    },
    ["attackRange_2"] = {
        ["center"] = 0, -- 0:自身  1:选中的当前目标自身
        ["range"] = "rangeInfo_2",
    },
    ["attackRange_3"] = {
        ["center"] = 0, -- 0:自身  1:选中的当前目标自身
        ["range"] = "rangeInfo_3",
    },
    ["attackRange_4"] = {
        ["center"] = 0, -- 0:自身  1:选中的当前目标自身
        ["range"] = "rangeInfo_4",
    },
    ["attackRange_5"] = {
        ["center"] = 0, -- 0:自身  1:选中的当前目标自身
        ["range"] = "rangeInfo_5",
    },
}

return AttackRange
