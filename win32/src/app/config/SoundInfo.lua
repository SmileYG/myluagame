local SoundInfo = {
    [1] = "Amb_Field_LP",
    [2] = "Game_Ponder_LP",
    [3] = "Game_Select_Character",
    [4] = "Game_Release_ToMove_Character",
}

return SoundInfo
