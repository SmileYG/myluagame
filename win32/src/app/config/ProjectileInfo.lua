local ProjectileInfo = {
    ["projt_1001"] = {
        ["res"] = "Projectile_Arrow-hd.png",
    },
    ["projt_1002"] = {
        ["res"] = "ArrowProjectile.png",
    },
    ["projt_1003"] = {
        ["res"] = "ShurikenProjectile-hd.png",
    },
    ["projt_1004"] = {
        ["res"] = "FireballProjectile-hd.png",
    },
}

return ProjectileInfo
