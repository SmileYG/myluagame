local SkillProcessInfo = {
    ["sp10001"] = {
        ["process_1"] = {
            ["execute"] = "doSkill",
            --["executor"] = "self",
            ["nextExecute"] = "skillEnd",
        },
        --[[
        ["process_2"] = {
            ["execute"] = "addSkillEffect",
            --["nextExecute"] = "executeEvent",
            ["nextExecute"] = "skillEnd",
        },
        ]]--
    },
    ["sp10002"] = {
        ["process_1"] = {
            ["execute"] = "doSkill",
            --["executor"] = "self",
            ["nextExecute"] = "skillEnd",
        },
        --[[
        ["process_2"] = {
            ["execute"] = "doHit",
            ["nextExecute"] = "skillEnd",
        },
        ]]--
    },
}

return SkillProcessInfo
