local Char = require("app.basicObj.Char")
local MapLayer = require("app.module.layer.MapLayer")
local CharLayer = require("app.module.layer.CharLayer")
local BattleUILayer = require("app.module.layer.BattleUILayer")
local BattleEffectLayer = require("app.module.layer.BattleEffectLayer")
--local TouchGridLayer = require("app.module.layer.TouchGridLayer")
local TouchLayer = require("app.module.layer.TouchLayer")
local MapInfo = require("app.module.data.MapInfo")
local MapInfoConst = require("app.const.MapInfoConst")
local BattleMgr = require("app.manager.BattleMgr")
local SkillMgr = require("app.manager.SkillMgr")
local BufferMgr = require("app.manager.BufferMgr")
local EffectMgr = require("app.manager.EffectMgr")
local BasicObjConst = require("app.const.BasicObjConst")
--local Scheduler = require("framework.scheduler")

local BattleScene = class("BattleScene", function()
    return display.newScene("BattleScene")
end)

-- touchGridLayer zOrder is the highest
local TOUCH_LAYER_ZORDER = 100
local BATTLEUI_LAYER_ZORDER = 90

function BattleScene:ctor()
    --[[
    Scheduler.scheduleUpdateGlobal(function()
        print("scheduleUpdate!!!!!!!!!!!!!!!!")
    end)
    ]]--
    --[[
    cc.ui.UILabel.new({
            UILabelType = 2, text = "Hello, World", size = 64})
        :align(display.CENTER, display.cx, display.cy)
        :addTo(self)
        ]]--
end

function BattleScene:onEnter()
    self:initScene()
end

function BattleScene:initScene()
    local sp = display.newSprite("bg/GameBoard03-hd.png")
    --local sp = display.newSprite("bg/heroGameBg.png")
    sp:setPosition(display.cx, display.cy)
    self:addChild(sp)

    local mapInfo = MapInfo.new()
    InstanceMgr.setIns(InstanceMgr.MAPINFO, mapInfo)

    self.mapLayer = MapLayer.new()
    self:addChild(self.mapLayer)
    InstanceMgr.setIns(InstanceMgr.MAPLAYER, self.mapLayer)

    --[[
    self.touchGridLayer = TouchGridLayer.new()
    self:addChild(self.touchGridLayer, 90)
    InstanceMgr.setIns(InstanceMgr.TOUCHGRIDLAYER, self.touchGridLayer)
    ]]--

    self.charLayer = CharLayer.new()
    self:addChild(self.charLayer)
    InstanceMgr.setIns(InstanceMgr.CHARLAYER, self.charLayer)

    self.battleUILayer = BattleUILayer.new()
    self:addChild(self.battleUILayer)
    InstanceMgr.setIns(InstanceMgr.BATTLEUILAYER, self.battleUILayer)

    self.battleEffectLayer = BattleEffectLayer.new()
    self:addChild(self.battleEffectLayer)
    InstanceMgr.setIns(InstanceMgr.BATTLEEFFECTLAYER, self.battleEffectLayer)

    self.touchLayer = TouchLayer.new()
    self:addChild(self.touchLayer, TOUCH_LAYER_ZORDER)
    InstanceMgr.setIns(InstanceMgr.TOUCHLAYER, self.touchLayer)

    self.skillMgr = SkillMgr.new()
    InstanceMgr.setIns(InstanceMgr.SKILLMGR, self.skillMgr)

    self.bufferMgr = BufferMgr.new()
    InstanceMgr.setIns(InstanceMgr.BUFFERMGR, self.bufferMgr)

    self.effectMgr = EffectMgr.new()
    InstanceMgr.setIns(InstanceMgr.EFFECTMGR, self.effectMgr)

    self.battleMgr = BattleMgr.new()

    --[[
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("AM" .. ".ExportJson")
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("BB" .. ".ExportJson")
    local s1 = ccs.Armature:create("BB")
    self.charLayer:addChild(s1)
    s1:setPosition(300, 300)

    local s2 = ccs.Armature:create("BB")
    self.charLayer:addChild(s2)
    s2:setPosition(400, 400)
    s2:getAnimation():play("stand")
    do return end
    ]]--
    local char = Char.new(10001, 1)
    char:setGridIndex({x = 1, y = 1})
    char:setInitDir(BasicObjConst.DIR_RIGHT)
    --char:setForce(MapInfoConst.FORCE_OWN)
    char:setForce(MapInfoConst.FORCE_ENEMY)
    char:getAnimation():play("stand")
    self.charLayer:addChar(char)

    local char1 = Char.new(10002, 2)
    local grid = {x = 2, y = 3}
    char1:setGridIndex(grid)
    char1:setInitDir(BasicObjConst.DIR_RIGHT)
    --char1:setForce(MapInfoConst.FORCE_OWN)
    char1:setForce(MapInfoConst.FORCE_ENEMY)
    char1:getAnimation():play("stand")
    self.charLayer:addChar(char1)

    local char2 = Char.new(10003, 2)
    local grid = {x = 3, y = 2}
    char2:setGridIndex(grid)
    char2:setInitDir(BasicObjConst.DIR_RIGHT)
    char2:getAnimation():play("stand")
    --char2:setForce(MapInfoConst.FORCE_OWN)
    char2:setForce(MapInfoConst.FORCE_ENEMY)
    self.charLayer:addChar(char2)

    local char3 = Char.new(10005, 2)
    local grid = {x = 2, y = 4}
    char3:setGridIndex(grid)
    char3:setInitDir(BasicObjConst.DIR_RIGHT)
    char3:getAnimation():play("stand")
    --char3:setForce(MapInfoConst.FORCE_OWN)
    char3:setForce(MapInfoConst.FORCE_ENEMY)
    self.charLayer:addChar(char3)

    local char4 = Char.new(10006, 2)
    local grid = {x = 1, y = 5}
    char4:setGridIndex(grid)
    char4:setInitDir(BasicObjConst.DIR_RIGHT)
    char4:getAnimation():play("stand")
    --char4:setForce(MapInfoConst.FORCE_OWN)
    char4:setForce(MapInfoConst.FORCE_ENEMY)
    self.charLayer:addChar(char4)

    ------------------ENEMY-----------------------
    local char5 = Char.new(10007, 2)
    local grid = {x = 9, y = 1}
    char5:setGridIndex(grid)
    char5:setInitDir(BasicObjConst.DIR_LEFT)
    char5:getAnimation():play("stand")
    --char5:setForce(MapInfoConst.FORCE_ENEMY)
    char5:setForce(MapInfoConst.FORCE_OWN)
    self.charLayer:addChar(char5)

    local char6 = Char.new(10008, 2)
    local grid = {x = 8, y = 2}
    char6:setGridIndex(grid)
    char6:setInitDir(BasicObjConst.DIR_LEFT)
    char6:getAnimation():play("stand")
    --char6:setForce(MapInfoConst.FORCE_ENEMY)
    char6:setForce(MapInfoConst.FORCE_OWN)
    self.charLayer:addChar(char6)

    local char7 = Char.new(10013, 2)
    local grid = {x = 7, y = 3}
    char7:setGridIndex(grid)
    char7:setInitDir(BasicObjConst.DIR_LEFT)
    char7:getAnimation():play("stand")
    --char7:setForce(MapInfoConst.FORCE_ENEMY)
    char7:setForce(MapInfoConst.FORCE_OWN)
    self.charLayer:addChar(char7)

    local char8 = Char.new(10010, 2)
    local grid = {x = 8, y = 4}
    char8:setGridIndex(grid)
    char8:setInitDir(BasicObjConst.DIR_LEFT)
    char8:getAnimation():play("stand")
    --char8:setForce(MapInfoConst.FORCE_ENEMY)
    char8:setForce(MapInfoConst.FORCE_OWN)
    self.charLayer:addChar(char8)

    local char9 = Char.new(10011, 2)
    local grid = {x = 9, y = 5}
    char9:setGridIndex(grid)
    char9:setInitDir(BasicObjConst.DIR_LEFT)
    char9:getAnimation():play("stand")
    --char9:setForce(MapInfoConst.FORCE_ENEMY)
    char9:setForce(MapInfoConst.FORCE_OWN)
    self.charLayer:addChar(char9)
end

function BattleScene:onExit()
end

return BattleScene
