local BasicObjConst = require("app.const.BasicObjConst")
local MapInfoConst = require("app.const.MapInfoConst")

local BasicContainer = class("BasicContainer", function()
    return display.newNode()
end)

function BasicContainer:ctor()
    self:setScale(BasicObjConst.SCALE_TO_ORIGINAL)
end

-- 选中变大
function BasicContainer:doMagnify()
    self:stopDoScaleAtion()
    if self:getScale() == BasicObjConst.SCALE_TO_LARGE then return end

    self.action = cc.ScaleTo:create(BasicObjConst.SCALE_TIME, BasicObjConst.SCALE_TO_LARGE)
    self:runAction(self.action)
end

-- 取消选中还原大小
function BasicContainer:doRestore(callBack, ...)
    self:stopDoScaleAtion()
    if self:getScale() == BasicObjConst.SCALE_TO_ORIGINAL then return end

    self.action = cc.ScaleTo:create(BasicObjConst.SCALE_TIME, BasicObjConst.SCALE_TO_ORIGINAL)
    if callBack then
        local actions = {}
        actions[#actions + 1] = self.action

        local arg = {...}
        local func = cc.CallFunc:create(function()
            if #arg == 0 then 
                callBack()
            else
                callBack(unpack(arg))
            end
        end)
        actions[#actions + 1] = func
        local _action = transition.sequence(actions)
        self:runAction(_action)
    else
        self:runAction(self.action)
    end
end

function BasicContainer:stopDoScaleAtion()
    --if not self.action then return end
    self:stopAllActions()
    --self.action = nil
end

return BasicContainer
