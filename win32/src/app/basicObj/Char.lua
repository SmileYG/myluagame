local AnimationObj = require("app.basicObj.AnimationObj")
local CharConst = require("app.const.CharConst")
local BasicObjConst = require("app.const.BasicObjConst")
local HeroInfo = require("app.config.HeroInfo")
local MapInfoConst = require("app.const.MapInfoConst")
local BattleUtil = require("app.util.BattleUtil")
local BattleConst = require("app.const.BattleConst")
local BufferConst = require("app.const.BufferConst")
local EventMgr = require("app.manager.EventMgr")
local NormalAttackInfo = require("app.config.NormalAttackInfo")
local SkillInfo = require("app.config.SkillInfo")
local BattleData = require("app.module.data.BattleData")
local EventMgr = require("app.manager.EventMgr")

local Char = class("Char", AnimationObj)

local OWN_HP_BAR_RES = "res/ui/battleUI/HP_Green-hd.png"
local ENEMY_HP_BAR_RES = "res/ui/battleUI/HP_Red-hd.png"
local BAR_BACKGROUND_RES = "res/ui/battleUI/HP_Background-hd.png"
local RED_FONT = "res/font/damageflyout.fnt"
local GREED_FONT = "res/font/healingflyout.fnt"

local table_insert = table.insert
local table_remove = table.remove
local math_max = math.max
local math_min = math.min
local ipairs = ipairs
local pairs = pairs

local MAX_CP = 100
--[[
local BUFFER_PROP_MAPPING = {
    [BufferConst.PROP_DAMAGE] = CharConst.PROP_HP,
    [BufferConst.PROP_HP] = CharConst.PROP_HP,
    [BufferConst.PROP_STR] = CharConst.PROP_STR,
    [BufferConst.PROP_DEF] = CharConst.PROP_DEF,
    [BufferConst.PROP_CP] = CharConst.PROP_CP,
    [BufferConst.PROP_MOV] = CharConst.PROP_MOV,
}
]]--

function Char:ctor(heroId, id)
    Char.super.ctor(self, BasicObjConst.TYPE_CHAR, heroId)
    self.id = id
    self.heroId = heroId

    
    --self.bufferList = {p = {}, np = {}}
    self.continuousBufferList = {}
    self.immediateBufferList = {}

    --self.usedBufferIdList = {}
    self.curWork = 0
    self.checkOff = false
    
    self.profession = HeroInfo[self.heroId]["profession"]
    if HeroInfo[self.heroId]["helpOthers"] == 0 then
        self.helpOthers = false
    else
        self.helpOthers = true
    end
    --self:setCurHp(HeroInfo[self.heroId][CharConst.PROP_HP])
    self.curHp = HeroInfo[self.heroId][CharConst.PROP_HP]
    self:setCurCp(HeroInfo[self.heroId][CharConst.PROP_CP])
    self:addChild(self.armature)
    self:createHpBar()
    self:updateZOrder()
end

function Char:regEvent()
    --EventMgr.regEvent(EventType.ATTACK_HIT, self.doHitHandler, nil, self)
end

function Char:doHitHandler()
    self:doHit()
    self:pullBuffer()
end

-- 生成血条
function Char:createHpBar()
    local res = nil
    local force = self:getForce()
    if force == MapInfoConst.FORCE_OWN then
        res = OWN_HP_BAR_RES
    elseif force == MapInfoConst.FORCE_ENEMY then
        res = ENEMY_HP_BAR_RES
    end

    if not self.backgroundBar then
        self.backgroundBar = display.newSprite(BAR_BACKGROUND_RES)
        self.hpBar = display.newSprite(res)
        self.backgroundBar:setAnchorPoint(0, 0)
        self.hpBar:setAnchorPoint(0, 0)
        self:addChild(self.backgroundBar)
        self:addChild(self.hpBar)
        
        local p = self:getBoneNodePositionByName(BasicObjConst.POINT_HP)
        self.backgroundBar:setPosition(p.x, p.y)
        self.hpBar:setPosition(p.x, p.y)
    else
        self.hpBar:setTexture(res)
    end
    self:updateHpBarPosition()
end

-- 调整血条位置
function Char:updateHpBarPosition()
    local size = self.backgroundBar:getContentSize()
    local x1, y1 = self.backgroundBar:getPosition()
    local _x = x1 - size.width / 2
    local _y = y1 - size.height / 2
    self.backgroundBar:setPosition(_x, _y)
    self.hpBar:setPosition(_x, _y)
end

-- 更新血条值
function Char:updateHpBar(value)
    local rate = value / self:getBA("HP")
    local toValue = math.max(self.hpBar:getScaleX() + rate, 0)
    toValue = math.min(toValue, 1)
    local action = cc.ScaleTo:create(0.2, toValue, 1)
    self.hpBar:runAction(action)
end

function Char:setHpBarVisible(bool)
    Char.super.setHpBarVisible(bool)
    if not self.backgroundBar then return end

    self.backgroundBar:setVisible(bool)
    self.hpBar:setVisible(bool)
end

function Char:updateBuffer()
    self.curWork = self.curWork + 1
    if #self.continuousBufferList == self.curWork then 
        self.checkOff = true
    end

    -- buffer工作
    local buffer = self.continuousBufferList[self.curWork]
    buffer:setRounds(buffer:getRounds() - 1)
    self:bufferWork(buffer, false)

    if buffer:getRounds() == 0 then
        self:removeBuffer(buffer:getId())
    end
end

function Char:pushBuffer(buffer)
    local bufferType = buffer:getBufferType()
    if bufferType == BufferConst.TYPE_ITEM then
        self:bufferWork(buffer)
        self:playBufferEffect(buffer)
    elseif bufferType == BufferConst.TYPE_ATTACK then
        table_insert(self.immediateBufferList, buffer)
    end
    --if buffer:getRounds() == 0 then
    --else
        --table_insert(self.continuousBufferList, buffer)
    --end
end

function Char:pullBuffer()
    local length = #self.immediateBufferList
    for i = 1, length do
        local buffer = self.immediateBufferList[i]
        self:bufferWork(buffer)
        self:playBufferEffect(buffer)
        self.immediateBufferList[i] = nil
    end
    self.immediateBufferList = {}
end

--[[
function Char:addBuffer(buffer)
    self:playBufferEffect(buffer)

    if buffer:getRounds() == 0 then
        self:bufferWork(buffer)
        return
    end
    table_insert(self.continuousBufferList, buffer)
end
]]--

function Char:removeBuffer(id)
    for index, buffer in ipairs(self.bufferList) do
        if buffer:getId() == id then
            table_remove(self.bufferList, index)
            return
        end
    end
end

function Char:checkBuffer()
end

function Char:bufferWork(buffer)
    local propType = buffer:getPropType()
    local value = buffer:getFinalValue()
    if propType == BufferConst.PROP_HP or propType == BufferConst.PROP_DAMAGE then
        self:hpBuffer(value, buffer:getEffectId())
    elseif propType == BufferConst.PROP_CP then
        self:cpBuffer(value)
    end
end

--[[
function Char:calHpFinalValue(buffer)
    local fa1 = BattleUtil.getCharFA(buffer:getReId(), buffer:getType())
    local fa2 = self:getFA("DEF")
    print("calHpFinalValue:",fa1,fa2)
    return math_max(fa1 * fa2 / 100, 0)
end
]]--

function Char:updateCurHp(value)
    local v = self:getCurHp() + value
    v = math.max(v, 0)
    v = math.min(v, self:getBA(CharConst.PROP_HP))
    --self:setCurHp(v)
    self.curHp = v
    if v == 0 then
        --self:setDeath(true)
        self:doDeath()
    end
end

function Char:updateCurCp(value)
    local v = self:getCurCp() + value
    self:setCurCp(v)
end 

function Char:hpBuffer(value, effectId)
    self:updateCurHp(value)
    self:updateHpBar(value)
    self:flyOutResult(value)
    --self:playBufferEffect(effectId)
end

function Char:cpBuffer(value)
    self:updateCurCp(value)
    self:flyOutResult(value)
end

function Char:playBufferEffect(buffer)
    if not buffer then return end

    local effectId = buffer:getEffectId()
    local mountPointName = buffer:getMountPointName()
    local effectMgr = InstanceMgr.getIns(InstanceMgr.EFFECTMGR)
    local mountPoint = self:getBoneNodePositionByName(mountPointName)
    local effect = effectMgr:createEffect(effectId, BattleConst.NORMAL_EFFECT, self, mountPoint)
    effect:play()
end

function Char:flyOutResult(value)
    if self.valueLabel then
        self.valueLabel:removeFromParent()
        self.valueLabel = nil
    end

    local _font = RED_FONT
    if value > 0 then
        value = "+" .. value
        _font = GREED_FONT
    end

    local p = self:getBoneNodePositionByName(BasicObjConst.POINT_HP)
    self.valueLabel = ccui.TextBMFont:create()
    self.valueLabel:setFntFile(_font)
    self.valueLabel:setString(value)
    self.valueLabel:setPosition(cc.p(p.x, p.y + 20))
    self:addChild(self.valueLabel)
    transition.execute(self.valueLabel, cc.MoveBy:create(0.2, cc.p(0, 20)), {
    easing = "elasticOut",
    onComplete = function()
        self.scheduleId = cc.Director:getInstance():getScheduler():scheduleScriptFunc(
        function()
            if not self.scheduleId then return end
            cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.scheduleId)
            self.scheduleId = nil
            if self.valueLabel then
                self.valueLabel:removeFromParent()
                self.valueLabel = nil
            end
        end, 
        0.5, false)
    end})
end

function Char:getAttackMode()
    local mode = nil
    if self.curCp ~= MAX_CP then
        mode = CharConst.ATTACK_MODE_NORMAL
    else
        mode = CharConst.ATTACK_MODE_SKILL
    end
    return mode
end

function Char:doStand()
    if self.initDir ~= self.dir then
        self:turnBack()
    end
    self:updateZOrder()
    self.curAction = CharConst.ACTION_STAND
    self.animation:play(CharConst.ACTION_STAND)
    EventMgr.trigEvent(EventType.UNSET_DOING_ID, self.id)
end

function Char:doAttack()
    self.curAction = CharConst.ACTION_ATTACK
    self.animation:play(CharConst.ACTION_ATTACK)
    EventMgr.trigEvent(EventType.SET_DOING_ID, self.id)
end

function Char:doHit()
    self.curAction = CharConst.ACTION_HIT
    self.animation:play(CharConst.ACTION_HIT)
end

function Char:doMove()
    self.curAction = CharConst.ACTION_MOVE
    self.animation:play(CharConst.ACTION_MOVE)
    EventMgr.trigEvent(EventType.SET_DOING_ID, self.id)
end

function Char:doDeath()
    self.curAction = CharConst.ACTION_DEATH
    self.animation:play(CharConst.ACTION_DEATH)
    EventMgr.trigEvent(EventType.UNSET_DOING_ID, self.id)
end

function Char:doSkill()
    self.curAction = CharConst.ACTION_SKILL
    self.animation:play(CharConst.ACTION_SKILL)
    EventMgr.trigEvent(EventType.SET_DOING_ID, self.id)
end

-- 基础属性
function Char:getBA(propType)
    local v = HeroInfo[self.heroId][propType]
    return v
end

--[[
function Char:getDamage(_type)
    local p = {}
    local np = {}
    for i, buffer in ipairs(self.bufferList) do
        local bType = buffer:getType()
        if bType == _type then
            if buffer:isPercent() then
                table_insert(p, buffer)
            else
                table_insert(np, buffer)
            end
        end
    end

    local sp = self:addValue(p)
    local snp = self:addValue(np)
    local b = self:getBA(_type)
    print("aaaaa:",b,self:getHeroId(),_type)
    return b * (1 + sp / 100) + snp
    --return self:getSumProp(BufferConst.PROP_DAMAGE)
end
]]--

-- 基础属性+其他加成
function Char:getFA(_type)
    --[[
    local v 
    if _type == "damage" then
        v = self:getSumProp(_type)
    else
        v = self:getSumProp(_type)
    end
    ]]--
    return self:getSumProp(_type)
end

function Char:getSumProp(_type)
    local np = {}
    local _t = "PROP_" .. _type
    for i, buffer in ipairs(self.continuousBufferList) do
        local bType = buffer:getPropType()
        if bType == _t then
            table_insert(np, buffer)
        end
    end

    local snp = self:addValue(np)
    local b = self:getBA(_type)
    print("aaaaa:",b,snp,self:getHeroId(),_type)
    return b + snp
    --[[
    -- p:buffer值为百分比的列表
    -- np:buffer值不是百分比的列表
    local p = {}
    local np = {}
    local _t = "PROP_" .. _type
    for i, buffer in ipairs(self.continuousBufferList) do
        local bType = buffer:getType()
        if bType == _t then
            if buffer:isPercent() then
                table_insert(p, buffer)
            else
                table_insert(np, buffer)
            end
        end
    end

    local sp = self:addValue(p)
    local snp = self:addValue(np)
    local b = self:getBA(_type)
    return b * (1 + sp / 100) + snp
    ]]--
end

function Char:addValue(list)
    local s = 0
    for _, buffer in ipairs(list) do
        s = s + buffer:getValue()
    end
    return s
end

--[[
function Char:setinitGridIndex(gridIndex)
    self.initGridIndex = gridIndex
end
]]--

function Char:setZOrder(value)
    self:setLocalZOrder(value)
end

function Char:updateZOrder()
    local gridIndex = self:getGridIndex()
    local index = gridIndex.x + gridIndex.y * MapInfoConst.GRID_HOR
    self:setZOrder(index)
end

function Char:getInitGridIndex()
    return BattleData.getInitPosById(self.id)
end

function Char:getGridIndex()
    return BattleData.getGridIndexById(self.id)
end

-- 唯一id
--[[
function Char:setId(id)
    self.id = id
end
]]--

function Char:getId()
    return self.id
end

--[[
function Char:setHeroId(id)
    self.heroId = id
end
]]--

function Char:getHeroId()
    return BattleData.getHeroIdById(self.id)
end

function Char:setCurHp(value)
    self.curHp = value
    local rate = value / self:getBA("HP")
    local toValue = math.max(rate, 0)
    self.hpBar:setScaleX(toValue)
end

function Char:getCurHp()
    return self.curHp
end

function Char:setCurCp(value)
    self.curCp = math.min(value, MAX_CP)
end

function Char:getCurCp()
    return self.curCp
end

function Char:clearCp()
    self:setCurCp(0)
end

function Char:setProfession(_type)
    self.profession = _type
end

function Char:getProfession()
    return self.profession
end

function Char:setPathInfo(pathInfo)
    self.pathInfo = pathInfo
end

function Char:getPathInfo()
    return self.pathInfo
end

-- 势力
--[[
function Char:setForce(force, gridIndex)
    self.force = force
    self:changeHpBar()
    gridIndex = gridIndex or self.gridIndex
    InstanceMgr.getIns(InstanceMgr.MAPINFO):setAreaTypeByGridIndex(gridIndex, force)
end
]]--

function Char:getForce()
    return BattleData.getForceById(self.id)
end

--[[
function Char:setDeath(bool)
    self.death = bool
end
]]--

function Char:isDeath()
    local realHp = BattleData.getHpById(self.id)
    if realHp > 0 then
        return false
    else
        return true
    end
end

function Char:setAttackTargetsGridIndex(targetsGridIndex)
    self.attackTargetsGridIndex = targetsGridIndex
end

function Char:getAttackTargetsGridIndex()
    return self.attackTargetsGridIndex
end

function Char:setHelpTargetsGridIndex(targetsGridIndex)
    self.helpTargetsGridIndex = targetsGridIndex
end

function Char:getHelpTargets()
    return self.helpTargetsGridIndex
end

function Char:canHelpOthers()
    return self.helpOthers
end

function Char:getBaseAttr(attrType)
    return HeroInfo[self.heroId][attrType]
end

function Char:getBasicBuffer(_type)
    return HeroInfo[self.heroId][_type]
end

function Char:getAttackInfo()
    return NormalAttackInfo[HeroInfo[self.heroId]["attack"]]
end

function Char:getSkillInfo()
    return SkillInfo[HeroInfo[self.heroId]["skill"]]
end

function Char:getSelectRangeId()
    return HeroInfo[self.heroId]["selectRange"]
end

function Char:getNRangeType()
    return HeroInfo[self.heroId]["n_range"]
end

function Char:getSRangeType()
    return HeroInfo[self.heroId]["s_range"]
end

function Char:getAttackType()
    return HeroInfo[self.heroId]["attackType"]
end

function Char:isCheckOff()
    return self.checkOff
end

function Char:resetCurWork()
    self.curWork = 0
end

function Char:clearAllTargetsGridIndex()
    self.pathInfo = nil
    self.attackTargetsGridIndex = nil
    self.helpTargetsGridIndex = nil
end

return Char
