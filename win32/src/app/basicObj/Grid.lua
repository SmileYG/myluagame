local MapInfoConst = require("app.const.MapInfoConst")
local EventMgr = require("app.manager.EventMgr")
local EventType = require("app.const.EventType")

local Grid = class("Grid", function()
    return display.newSprite()
end)

local RES = "bg/MovementGrid-hd.png"

function Grid:ctor(gridIndexX, gridIndexY)
    local scaleX = MapInfoConst.X_INFO[gridIndexY][gridIndexX]["scaleX"]
    local scaleY = MapInfoConst.HOR_SCALE_Y[gridIndexY]
    local skewX = MapInfoConst.COLUMN_SKEW_X[gridIndexX]
    local _x = MapInfoConst.X_INFO[gridIndexY][gridIndexX]["x"]
    local _y = MapInfoConst.POS_Y[gridIndexY]

    self.gridIndex = {x = gridIndexX, y = gridIndexY}
    self:setTexture(RES)
    self:setAnchorPoint(0, 0)
    self:setScaleX(scaleX)
    self:setScaleY(scaleY)
    self:setRotationSkewX(skewX)
    --if data.skewY then self:setRotationSkewY(data.skewY) end
    self:setPosition(_x, _y)
    self:retain()

    --self:initEvent()
end

--[[
function Grid:initEvent()
    local function onTouchEvent(event)
        if event.name == "began" then
            return true
        elseif event.name == "ended" then
            --Event.trigEvent(EventType.TOUCH_ENDED_GRID, self.pos)
            print("grid ended.....")
        end
    end
    self:setTouchEnabled(true)
    self:addNodeEventListener(cc.NODE_TOUCH_EVENT, onTouchEvent)
end
]]--

function Grid:getGridIndex()
    return self.gridIndex
end

return Grid
