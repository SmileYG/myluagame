local MapInfoConst = require("app.const.MapInfoConst")
local EventMgr = require("app.manager.EventMgr")
local EventTpye = require("app.const.EventType")

local TouchGrid = class("TouchGrid", function()
    return display.newNode()
end)

local GRID_HD_WIDTH = 106
local GRID_HD_HEIGHT = 105
local math_floor = math.floor

function TouchGrid:ctor(gridIndexX, gridIndexY)
    local scaleX = MapInfoConst.X_INFO[gridIndexY][gridIndexX]["scaleX"]
    local scaleY = MapInfoConst.HOR_SCALE_Y[gridIndexY]
    local skewX = MapInfoConst.COLUMN_SKEW_X[gridIndexX]
    local _x = MapInfoConst.X_INFO[gridIndexY][gridIndexX]["x"]
    local _y = MapInfoConst.POS_Y[gridIndexY]
    local standPosX = math_floor(GRID_HD_WIDTH / 2)
    local standPosY = math_floor(GRID_HD_HEIGHT / 2 * 0.6)

    self.gridIndex = {x = gridIndexX, y = gridIndexY}
    self.standPos = {x = standPosX, y = standPosY}
    self.curRect = cc.rect(_x, _y, GRID_HD_WIDTH * scaleX, GRID_HD_HEIGHT * scaleY)
    self:setAnchorPoint(0, 0)
    self:setContentSize(cc.size(GRID_HD_WIDTH, GRID_HD_HEIGHT + 10))
    self:setScaleX(scaleX)
    self:setScaleY(scaleY)
    self:setRotationSkewX(skewX)
    --if data.skewY then self:setRotationSkewY(data.skewY) end
    self:setPosition(_x, _y)
    self:retain()

    self:initEvent()
end

function TouchGrid:initEvent()
    local function onTouchEvent(event)
        if event.name == "began" then
            --self.charCanRestore = false
            --EventMgr.trigEvent(EventType.TOUCH_BEGAN_GRID, self.pos)
            print("grid touch began....,")
            return true
        elseif event.name == "ended" then
            EventMgr.trigEvent(EventType.TOUCH_ENDED_GRID, self.gridIndex)
            print("grid touch ended.....",self.gridIndex.x, self.gridIndex.y)
        --[[
        elseif event.name == "moved" then
            self.charCanRestore = true
            print("touchgrid move.....")
        ]]--
        end
    end
    self:setTouchSwallowEnabled(false)
    self:setTouchEnabled(true)
    self:addNodeEventListener(cc.NODE_TOUCH_EVENT, onTouchEvent)
end

function TouchGrid:getGridIndex()
    return self.gridIndex
end

function TouchGrid:getCurRect()
    return self.curRect
end

function TouchGrid:getStandPos()
    return self.standPos
end

function TouchGrid:getStandPosToWorldSpace()
    return self:convertToWorldSpace(cc.p(self.standPos.x, self.standPos.y))
end

return TouchGrid
