local DelayEffect = class("DelayEffect")
local DelayEffectInfo = require("app.config.DelayEffectInfo")
local BattleUtil = require("app.util.BattleUtil")

local string_format = string.format

function DelayEffect:ctor(id, parent, point)
    self.curFrame = 0
    self.container = display.newNode()
    self.parent = parent
    self.info = DelayEffectInfo[id]
    self.frameNums = self.info["framesNum"]
    self.delayTimeInfo = self.info["framesDelayTime"]
    local executeFrame = self.info["executeFrame"]
    if executeFrame then self:setEventCallFuncByFrame(executeFrame) end

    local plistName = self.info["plistName"]
    cc.SpriteFrameCache:getInstance():addSpriteFrames(plistName)
    self.prefix = string.split(plistName, ".")[1]
    
    --[[
    local _p = self.info["parent"]
    self._parent = nil
    local _point = nil
    if _p == 0 then
        self._parent = InstanceMgr.getIns(InstanceMgr.BATTLEEFFECTLAYER)
        _point = point
    else
        self._parent = parent
        _point = parent:getBoneNodePositionByName(self.info["mountPointName"])
    end
    ]]--

    if self.info["anchorPoint"] then
        self.container:setAnchorPoint(cc.p(self.info["anchorPoint"][1], self.info["anchorPoint"][2]))
    end
    self.parent:addChild(self.container)
    --self.container:setPosition(_point)
    self.container:setPosition(point)

    --[[
    if iskindof(_parent, "Char") then
        local p = _parent:getBoneNodePositionByName(self.info["mountPointName"])
        self.container:setPosition(p)
    end
    ]]--

    self:parseFrameDelayTime()
    return self.container
end

function DelayEffect:parseFrameDelayTime()
    self.framesDelayTime = {}
    local time = 0
    for i = 1, self.frameNums do
        if self.delayTimeInfo[i] then
            time = self.delayTimeInfo [i]
        end
        self.framesDelayTime[i] = time
    end
end

function DelayEffect:setEventCallFuncByFrame(frame)
    self.eventFrame = frame
end

function DelayEffect:play()
    self.curFrame = 0
    self:update()
    -- todo play sound
end

function DelayEffect:update()
    self.curFrame = self.curFrame + 1
    if not self.framesDelayTime[self.curFrame] then 
        if not toBooleanEx(self.info.loop) then
            self:completed()
            return
        else
            self.curFrame = 1
        end
    end

    self:checkEvent()
    self:switchTexture()
    self:doSchedule()
end

function DelayEffect:checkEvent()
end

function DelayEffect:switchTexture()
    local imgName = string_format("%s_%d.png", self.prefix, self.curFrame)
    --print("switchTexture:",imgName)
    if not self.spriteFrame then
        local spriteFrame = cc.SpriteFrameCache:getInstance():getSpriteFrame(imgName)
        self.spriteFrame = cc.Sprite:createWithSpriteFrame(spriteFrame)

        if self.info["anchorPoint"] then
            self.spriteFrame:setAnchorPoint(cc.p(self.info["anchorPoint"][1], self.info["anchorPoint"][2]))
        end 
        self.container:addChild(self.spriteFrame)
    end
    self.spriteFrame:setSpriteFrame(imgName)
end

function DelayEffect:doSchedule()
    local callFunc = function()
        --print("iiiiiiiiiiiiiiiiiiiiiaa:",self.curFrame)
        self:stop()
        self:update()
    end

    self.scheduleId = cc.Director:getInstance():getScheduler():scheduleScriptFunc(callFunc, self.framesDelayTime[self.curFrame], false)
    --self.action = self.container:performWithDelay(callFunc, self.framesDelayTime[self.curFrame])
end

function DelayEffect:completed()
    self:stop()
    self.container:removeFromParent()
end

function DelayEffect:stop()
    if not self.scheduleId then return end
    cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.scheduleId)
    self.scheduleId = nil
    --self.container:stopAction(self.action)
end

function DelayEffect:getAnimation()
    return self.container
end

return DelayEffect
