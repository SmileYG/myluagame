--local Basic = require("app.basicObj.Basic")
local BattleItem = class("BattleItem", function()
    return display.newSprite()
end)
local BasicObjConst = require("app.const.BasicObjConst")
local BattleItemInfo = require("app.config.BattleItemInfo")

local MAX_ZORDER = 10
local MIN_ZORDER = 0

function BattleItem:ctor(itemId)
    self.info = BattleItemInfo[itemId]
    self:setLocalZOrder(MIN_ZORDER)
    self:setTexture(self.info.res)
    self:setScale(BasicObjConst.SCALE_TO_ORIGINAL)
    self:float()
    --self:initEvent()
end

--[[
function BattleItem:initEvent()
    local function onTouchEvent(event)
        if event.name == "began" then
            --self:setTouchSwallowEnabled(true)
            print("BattleItem touch began:", event.x, event.y, event.prevX, event.prevY)
            return true
        elseif event.name == "ended" then
            --self:setTouchSwallowEnabled(false)
            print("BattleItem touch ended:", event.x, event.y, event.prevX, event.prevY)
        end
    end
    self:setTouchEnabled(true)
    self:addNodeEventListener(cc.NODE_TOUCH_EVENT, onTouchEvent)
end
]]--

function BattleItem:movingHandler(x, y)
    self:setPosition(x, y)
    self:setLocalZOrder(MAX_ZORDER)
end

-- 选中变大
function BattleItem:doMagnify()
    self:stopDoScaleAtion()
    self:float()
    if self:getScale() == BasicObjConst.ITEM_SCALE_TO_LARGE then return end

    local action = cc.ScaleTo:create(BasicObjConst.SCALE_TIME, BasicObjConst.ITEM_SCALE_TO_LARGE)
    self:runAction(action)
end

-- 取消选中还原大小
function BattleItem:doRestore(callBack, ...)
    self:stopDoScaleAtion()
    self:float()
    if self:getScale() == BasicObjConst.SCALE_TO_ORIGINAL then return end
    
    local _x, _y = self:getPosition()
    if _x ~= self.point.x or _y ~= self.point.y then
        self:goback()
    end

    local action = cc.ScaleTo:create(BasicObjConst.SCALE_TIME, BasicObjConst.SCALE_TO_ORIGINAL)
    if callBack then
        local actions = {}
        actions[#actions + 1] = action

        local arg = {...}
        local func = cc.CallFunc:create(function()
            if #arg == 0 then 
                callBack()
            else
                callBack(unpack(arg))
            end
        end)
        actions[#actions + 1] = func
        local _action = transition.sequence(actions)
        self:runAction(_action)
    else
        self:runAction(action)
    end
end

function BattleItem:goback()
    local action = cc.MoveTo:create(BasicObjConst.ITEM_GOBACK_TIME, self.point)
    self:runAction(action)
    self:setLocalZOrder(MIN_ZORDER)
end

-- 上下浮动
function BattleItem:float()
    local _x, _y = self:getPosition()
    local action = cc.MoveBy:create(1, cc.p(0, 5))
    local callback = action:reverse()
    local seq = cc.Sequence:create(action, callback)
    self:runAction(cc.RepeatForever:create(seq))
end

function BattleItem:stopDoScaleAtion()
    --if not self.action then return end
    self:stopAllActions()
    --self.action = nil
end

-- 在道具架上的位置
function BattleItem:setPositionOnTileRack(x, y)
    self.point = cc.p(x, y)
end

function BattleItem:setId(id)
    self.id = id
end

function BattleItem:getId()
    return self.id
end

function BattleItem:getBufferInfo()
    return self.info.buffer
end

function BattleItem:getTargetType()
    return self.info.targetType
end

return BattleItem
