local DelayEffect = require("app.basicObj.DelayEffect")
local EventDelayEffect = class("EventDelayEffect", DelayEffect)
local EventMgr = require("app.manager.EventMgr")

function EventDelayEffect:ctor(id, parent, point)
    EventDelayEffect.super.ctor(self, id, parent, point)
end

function EventDelayEffect:checkEvent()
    EventDelayEffect.super.checkEvent(self)

    if not self.eventFrame or self.curFrame ~= self.eventFrame then 
        return
    elseif self.curFrame == self.eventFrame then
        --print("trigger frame event!!!!!!!!!!!!!!!!!!!!!!!!!",self.curFrame)
        EventMgr.trigEvent(EventType.EFFECT_FRAME_EVENT, self.parent)
    end
end

return EventDelayEffect
