local BasicContainer = require("app.basicObj.BasicContainer")
local HeroInfo = require("app.config.HeroInfo")
local BasicObjConst = require("app.const.BasicObjConst")
local BattleUtil = require("app.util.BattleUtil")
local CharConst = require("app.const.CharConst")
local EventMgr = require("app.manager.EventMgr")

local AnimationObj = class("AnimationObj", BasicContainer)

local PATH = "res/char/"

local ATTACK_SIGN_RES = "res/ui/battleUI/AttackReticle2-hd.png"
local WAITING_SELECT_SIGN_RES = "res/ui/battleUI/AllyReticle01_InsideRing-hd.png"
local NO_SYMBOL_RES = "res/ui/battleUI/NO_Symbol-hd.png"

function AnimationObj:ctor(_type, animationId)
    AnimationObj.super.ctor(self)
    self._type = _type
    self.animationId = animationId
    self.signList = {}

    -- 默认方向是右
    self.dir = BasicObjConst.DIR_RIGHT
    local fileName = HeroInfo[animationId]["res"]

    self:loadRes(fileName)
    self.armature = ccs.Armature:create(fileName)
    self.animation = self.armature:getAnimation()
    self:initEvent()
end

function AnimationObj:loadRes(fileName)
    local fullName = self:getFullName(fileName)
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(fullName)
end

function AnimationObj:getFullName(fileName)
    return string.format("%s%s.ExportJson", PATH, fileName)
end

function AnimationObj:initEvent()
    local function onFrameEvent(bone, eventName, originFrameIndex, currentFrameIndex)
        if eventName == CharConst.ATTACK_FRAME_EVENT then
            print("attack.........")
            EventMgr.trigEvent(EventType.NORMAL_ATTACK_EVENT)
            --EventMgr.trigEvent(EventType.ATTACK_HIT)
        elseif eventName == CharConst.SKILL_FRAME_EVENT then
            print("skill............")
            EventMgr.trigEvent(EventType.SKILL_ATTACK_EVENT)
        elseif eventName == CharConst.SKILL_MULT_EVENT then
            print("skill MULT............")
            EventMgr.trigEvent(EventType.SKILL_MULT_EVENT)
        end
    end

    local function onMovementEvent(bone, eventName, actionId)
        if eventName == ccs.MovementEventType["complete"] then
            print("action complete...............",eventName,actionId)
            if self.curAction == actionId then
                if actionId == CharConst.ACTION_HIT then
                    --self:hitEnd()
                    self:doStand()
                elseif actionId == CharConst.ACTION_ATTACK then
                    self:doStand()
                elseif actionId == CharConst.ACTION_SKILL then
                    self:doStand()
                    EventMgr.trigEvent(EventType.SKILL_ACTION_END)
                elseif actionId == CharConst.ACTION_DEATH then
                    self:setHpBarVisible(false) 
                end
            end
        end
    end
    self.animation:setFrameEventCallFunc(onFrameEvent)
    self.animation:setMovementEventCallFunc(onMovementEvent)
end

function AnimationObj:getAnimation()
    return self.animation
end

function AnimationObj:getBoneByName(name)
    return self.armature:getBone(name)
end

function AnimationObj:getBoneNodePositionByName(name)
    local bone = self:getBoneByName(name)
    local p = bone:getNodePosition()
    p.x = p.x * self.armature:getScaleX()
    return p
end

function AnimationObj:setInitDir(dir)
    self.initDir = dir
    self:setDir(dir)
end

function AnimationObj:getInitDir()
    return self.initDir
end

function AnimationObj:getDir()
    return self.dir
end

-- 脸的朝向
function AnimationObj:setDir(dir)
    self.dir = dir
    self.armature:setScaleX(dir)

    --[[
    local gridIndex = self:getGridIndex()
    local desPosition = BattleUtil.getDestinationPosition(gridIndex, self)
    self:setPosition(desPosition)
    ]]--
end

-- 转身
function AnimationObj:turnBack()
    self:setDir(-1 * self.dir)
end

function AnimationObj:adjustPosition()
end

function AnimationObj:setHpBarVisible(bool)
end

function AnimationObj:showSign(sign)
    local bool, index = self:haveSign(sign)
    -- 是否当前sign
    if bool and index == 1 then
        return
    elseif not bool then
        table.insert(self.signList, 1, sign)
    end
    self:showSignHandler(sign)
end

function AnimationObj:showSignHandler(sign)
    self:stopDoSignAction()
    if sign == CharConst.CAN_BE_ATTACKED then
        self:createAttackReticle()
    elseif sign == CharConst.CAN_NOT_BE_ATTACKED then
        self:createNoSymbo()
    elseif sign == CharConst.WAITING_SELECT then
        self:createWaitingSelectSign()
    end
end

function AnimationObj:showSignAnimationHandler()
    local actions = {}
    local action_1 = cc.ScaleTo:create(BasicObjConst.SCALE_TIME, 1)
    actions[#actions + 1] = action_1

    local callFunc = cc.CallFunc:create(function()
        self.signAction = cc.RepeatForever:create(cc.RotateBy:create(BasicObjConst.ROTATE_INTERVAL, BasicObjConst.ROTATE_DEGREE))
        self.signSprite:runAction(self.signAction)
    end)
    actions[#actions + 1] = callFunc

    self.signAction = transition.sequence(actions)
    self.signSprite:runAction(self.signAction)
end

function AnimationObj:haveSign(sign)
    for i, _sign in ipairs(self.signList) do
        if _sign == sign then return true, i end
    end
    return false
end

function AnimationObj:removeCurSign()
    if #self.signList == 0 then return end

    table.remove(self.signList, 1)
    if #self.signList > 0 then
        self:showSignHandler(self.signList[1])
    else
        self:removeAllSign()
    end
end

function AnimationObj:removeAllSign()
    self.signList = {}
    self:hideSignAnimationHandler()
end

function AnimationObj:hideSignAnimationHandler()
    if not self.signSprite then return end

    local actions = {}
    local action_1 = cc.ScaleTo:create(BasicObjConst.SCALE_TIME, 1.5)
    actions[#actions + 1] = action_1

    local callFunc = cc.CallFunc:create(function()
        self:stopDoSignAction()
    end)
    actions[#actions + 1] = callFunc

    self.signAction = transition.sequence(actions)
    self.signSprite:runAction(self.signAction)
end

function AnimationObj:stopDoSignAction()
    if not self.signSprite then return end
    if self.signAction then
        self.signSprite:stopAction(self.signAction)
        self.signSprite:removeFromParent()
        self.signSprite = nil
        self.signAction = nil
    end
end

function AnimationObj:createAttackReticle()
    self.signSprite = display.newSprite(ATTACK_SIGN_RES)
    local bone = self:getBoneByName(BasicObjConst.POINT_SIGN)
    local p = bone:getNodePosition()
    self.signSprite:setPosition(p.x, p.y)
    self.signSprite:setScale(1.5)
    self:addChild(self.signSprite)
    self:showSignAnimationHandler()
end

function AnimationObj:createWaitingSelectSign()
    self.signSprite = display.newSprite(WAITING_SELECT_SIGN_RES)
    local bone = self:getBoneByName(BasicObjConst.POINT_SIGN)
    local p = bone:getNodePosition()
    self.signSprite:setPosition(p.x, p.y)
    self.signSprite:setScale(1.5)
    self:addChild(self.signSprite)
    self:showSignAnimationHandler()
end

function AnimationObj:createNoSymbo()
    self.signSprite = display.newSprite(NO_SYMBOL_RES)
    local bone = self:getBoneByName(BasicObjConst.POINT_SIGN)
    local p = bone:getNodePosition()
    self.signSprite:setPosition(p.x, p.y)
    self.signSprite:setScale(1.5)
    self:addChild(self.signSprite)
    self:showSignAnimationHandler()
end

function AnimationObj:showHealReticle()
    --HealReticle-hd.png
end

function AnimationObj:showAllyReticle()
    --AllyReticle01_OutsideRing-hd.png 例如：忍者可以交互的位置
end

function AnimationObj:showAttackReticle_2()
    --AttackReticle2-hd.png 例如：复活敌方尸体
end

function AnimationObj:hitEnd()
    EventMgr.trigEvent(EventType.HIT_END)
end

function AnimationObj:getType()
    return self._type
end

function AnimationObj:isAnimationObj()
    return true
end

return AnimationObj
