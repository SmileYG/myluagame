-- 禁止被clone的table

local BasicTable = {}
local mt = {}

mt.__index = function(tbl, key)
    if key == "canNotClone" then
        return true
    end
end

mt.__newindex = function(tbl, key, value)
    if key == "canNotClone" then
        print("This key of value can not change,look at app\"util\"OtherUtil.lua")
    elseif key == "mt" then
        print("error....")
    end
end

function BasicTable:ctor()
    setmetatable(BasicTable, mt)
end

return BasicTable
