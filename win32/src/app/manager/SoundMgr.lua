local SoundMgr = class("SoundMgr")
local SoundInfo = require("app.config.SoundInfo")

local SOUND_PATH = "res/sound/"
local SOUND_FORMAT = nil
local targetPlatform = cc.Application:getInstance():getTargetPlatform()

function SoundMgr:ctor()
    self:getSoundFormat()
end

function SoundMgr:getSoundFormat()
    if cc.PLATFORM_OS_MAC == targetPlatform or cc.PLATFORM_OS_IPHONE == targetPlatform or cc.PLATFORM_OS_IPAD == targetPlatform then
        SOUND_FORMAT = ".caf"
    elseif cc.PLATFORM_OS_ANDROID == targetPlatform then
        SOUND_FORMAT = ".ogg"
    else
        SOUND_FORMAT = ".mp3"
    end 
end

function SoundMgr:playMusic(nameId)
    do return end
    local fullName = self:getFullName(nameId)
    local id = ccexp.AudioEngine:play2d(fullName, true)
    return id
end

function SoundMgr:playSoundEffect(nameId)
    do return end
    local fullName = self:getFullName(nameId)
    local id = ccexp.AudioEngine:play2d(fullName)
    return id
end

function SoundMgr:preload(nameId)
    local fullName = self:getFullName(nameId)
    ccexp.AudioEngine:preload(fullName)
end

function SoundMgr:stopById(id)
    ccexp.AudioEngine:stop(id)
end

function SoundMgr:stopAll()
    ccexp.AudioEngine:stopAll()
end

function SoundMgr:uncache(fileName)
end

function SoundMgr:uncacheAll()
end

function SoundMgr:getFullName(nameId)
    local fileName = SoundInfo[nameId]
    local fullName = string.format("%s%s%s", SOUND_PATH, fileName, SOUND_FORMAT)
    return fullName
end

--ccexp.AudioEngine:preload("res/sound/Amb_Field_LP.mp3")
return SoundMgr
