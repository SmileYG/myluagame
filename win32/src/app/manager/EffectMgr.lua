local EffectMgr = class("EffectMgr")
local DelayEffect = require("app.basicObj.DelayEffect")
local EventDelayEffect = require("app.basicObj.EventDelayEffect")
local BattleConst = require("app.const.BattleConst")

function EffectMgr:ctor()
end

function EffectMgr:createEffect(effectId, _type, parent, point)
    local effect 
    if _type == BattleConst.EVENT_EFFECT then
        effect = EventDelayEffect.new(effectId, parent, point)
    elseif _type == BattleConst.NORMAL_EFFECT then
        effect = DelayEffect.new(effectId, parent, point)
    end
    return effect
end

return EffectMgr
