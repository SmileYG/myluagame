InstanceMgr = {}

InstanceMgr.MAPINFO = "mapInfo"

InstanceMgr.MAPLAYER = "mapLayer"
InstanceMgr.CHARLAYER = "charLayer"
InstanceMgr.BATTLEUILAYER = "battleUILayer"
InstanceMgr.TOUCHLAYER = "touchLayer"
InstanceMgr.BATTLEEFFECTLAYER = "battleEffectLayer"

InstanceMgr.BATTLEMGR = "battleMgr"
InstanceMgr.SKILLMGR = "skillMgr"
InstanceMgr.BUFFERMGR = "bufferMgr"
InstanceMgr.EFFECTMGR = "effectMgr"
InstanceMgr.SOUNDMGR = "soundMgr"

function InstanceMgr.setIns(insName, ins)
    InstanceMgr[insName] = ins
end

function InstanceMgr.getIns(insName)
    return InstanceMgr[insName]
end

--[[
function InstanceMgr.getMapInfo()
    return InstanceMgr.mapInfo
end

function InstanceMgr.setMapInfo(ins)
    InstanceMgr.mapInfo = ins
end

function InstanceMgr.setMapLayer(ins)
    InstanceMgr.mapLayer = ins
end

function InstanceMgr.getMapLayer()
    return InstanceMgr.mapLayer
end

function InstanceMgr.setCharLayer(ins)
    InstanceMgr.charLayer = ins
end

function InstanceMgr.getCharLayer()
    return InstanceMgr.charLayer
end

function InstanceMgr.setTouchLayer(ins)
    InstanceMgr.touchLayer = ins
end

function InstanceMgr.getTouchLayer()
    return InstanceMgr.touchLayer
end

function InstanceMgr.setSkillMgr(ins)
    InstanceMgr.skillMgr = ins
end

function InstanceMgr.getSkillMgr()
    return InstanceMgr.skillMgr
end
]]--

return InstanceMgr
