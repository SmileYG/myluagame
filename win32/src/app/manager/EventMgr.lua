local eventsTab = {}
local event = {}
event.PRIO_HIGH = 10
event.PRIO_MIDDLE = 5
event.PRIO_LOW = 1

local type = type
local ipairs = ipairs
local pairs = pairs
local unpack = unpack
local table_sort = table.sort
local string_format = string.format
local table_remove = table.remove
local table_insert = table.insert

function event.regEvent(eventType, func, priority, ...)
    -- todo: modify clone
    local priority = priority or event.PRIO_MIDDLE

    if eventType == nil then
        print("# Event type is nil !!!!!!!!!!!!!!!!!!")
        return
    end

    if type(func) ~= "function" then
        print(string_format("Event: trigger function error! eventType:%s", eventType))
        return
    end

    --if eventsTab == nil then eventsTab = {} end
    local eventFunc = { func, priority, {...} }

    if eventsTab[eventType] == nil then
        eventsTab[eventType] = {}
    end

    eventsTab[eventType][#eventsTab[eventType] + 1] = eventFunc
    -- sort by priority, bigger number, higher priority
    table_sort(eventsTab[eventType], function(t1, t2)
        return t1[2] > t2[2]
    end)
end

function event.unRegEvent(eventType, func)
    local eventFuncs = eventsTab[eventType]

    if eventFuncs == nil or #eventFuncs == 0 then
        print(string_format("Event: %s has no response function", eventType))
        return
    end

    for k, v in ipairs(eventFuncs) do
        if v[1] == func then
            table_remove(eventFuncs, k)
            return
        end
    end
end

function event.trigEvent(eventType, ...)
    -- clone() 防止回调函数时调用unRegEvent()把数据给清空了
    --print("trigEvent evnetType:" , eventType)
    local eventFuncs = cloneEx(eventsTab[eventType])
    if eventFuncs == nil or #eventFuncs == 0 then
        print(string_format("Event: %s trigger nothing", eventType))
        return
    end
    for k, v in ipairs(eventFuncs) do
        if event.hasRegEvent(eventType, v[1]) then
            if #{...} == 0 then
                v[1](unpack(v[3]))
            else
                local p = {}
                for _, pv in ipairs(v[3]) do
                    table_insert(p, pv)
                end

                for _, pv in ipairs({...}) do
                    table_insert(p, pv)
                end
                v[1](unpack(p))
            end
        end
    end
end

function event.hasRegEvent( eventType, func )
    local eventFuncs = eventsTab[eventType]

    if eventFuncs == nil or #eventFuncs == 0 then
        return false
    end

    for k, v in ipairs(eventFuncs) do
        if v[1] == func then
            return true
        end
    end

    return false
end

function event.clearEvent( eventType )
    print("\n\n\n\n\n\n clearEvent:",eventType)
    if eventsTab[eventType] then
        eventsTab[eventType] = {}
    end
end

function event.clearEvents()
    eventsTab = {}
end

return event
