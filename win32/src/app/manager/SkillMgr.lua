local SkillMgr = class("SkillMgr")
local SkillProcessInfo = require("app.config.SkillProcessInfo")
local EventMgr = require("app.manager.EventMgr")
local BattleUtil = require("app.util.BattleUtil")

function SkillMgr:ctor()
    self.mapInfo = InstanceMgr.getIns(InstanceMgr.MAPINFO)
    self.charLayer = InstanceMgr.getIns(InstanceMgr.CHARLAYER)
    self.mapLayer = InstanceMgr.getIns(InstanceMgr.MAPLAYER)
    self.touchLayer = InstanceMgr.getIns(InstanceMgr.CHARLAYER)
end

--[[
function SkillMgr:skillStart(info)
    self.info = info
    self.skillInfo = SkillInfo[info.attackerId]
    self.
    EventMgr.regEvent(EventType.SKILL_ACTION_END, self.callback, self)
    if self.skillInfo.ready then
        self:ready()
    else
        self:perform()
    end
end

function SkillMgr:perform()
end

function SkillMgr:ready()
end

function SkillMgr:skillEnd()
end
]]--

--[[
--info = {
--  skillId = xxxx,
--  attackerId = xxxx,
--  targets = {xxxx},
--}
--]]
function SkillMgr:skillStart(info)
    self.info = info
    self.process = SkillProcessInfo[info.skillId]
    self.processValue = 0
    self.callback = handler(self, SkillMgr.execute)
    self:execute()
end

function SkillMgr:execute()
    EventMgr.unRegEvent(EventType.SKILL_EXECUTE_EVENT, self.callback)
    self.processValue = self.processValue + 1
    self.key = "process_" .. self.processValue
    print("sssssssss:",self.key)
    self:processHandler(self.process[self.key]["execute"])

    local nextExecute = self.process[self.key]["nextExecute"]
    if not nextExecute then
        print("无需等待。。。。。")
        self:execute()
    elseif nextExecute == "executeEvent" then
        print("注册事件。。。。。。")
        EventMgr.regEvent(EventType.SKILL_EXECUTE_EVENT, self.callback)
        return
    elseif nextExecute == "skillEnd" then
        self:skillEnd()
        return
    end
end

function SkillMgr:processHandler(action)
    if action == "doSkill" then
        print("skill doSkill")
        EventMgr.trigEvent(EventType.DO_SKILL)
    --elseif action == "addSkillEffect" then
        --print("skill addSkillEffect")
        --EventMgr.trigEvent(EventType.ADD_SKILL_EFFECT)
    --elseif action == "doHit" then
        --EventMgr.trigEvent(EventType.DO_HIT)
    --elseif action == "addBuffer" then
        --print("skill  addBuffer")
    end
end

function SkillMgr:skillEnd()
    self.processValue = 0
    EventMgr.trigEvent(EventType.SKILL_END)
    print("技能流程结束。。。。。。")
end

return SkillMgr
