local BufferMgr = class("BufferMgr")
local BufferInfo = require("app.config.BufferInfo")
local BufferConst = require("app.const.BufferConst")
local BattleData = require("app.module.data.BattleData")

local BUFFER_CLASS_NAME = {
    [BufferConst.PROP_DAMAGE] = "DamageBuffer",
    [BufferConst.PROP_HP] = "Buffer",
    [BufferConst.PROP_STR] = "Buffer",
    [BufferConst.PROP_DEF] = "Buffer",
    [BufferConst.PROP_CP] = "Buffer",
    [BufferConst.PROP_MOV] = "Buffer",
}

local table_insert = table.insert

function BufferMgr:ctor()
    self.bufferList = {}
end

function BufferMgr:createBuffer(bName, charId)
    local bInfo = BufferInfo[bName]
    bInfo.id = #self.bufferList + 1
    local className = BUFFER_CLASS_NAME[bInfo.propType]
    local class = require("app.module.buffer." .. className)
    local buffer = class.new(bInfo)
    self.bufferList[bInfo.id] = buffer
    BattleData.setBufferIdById(charId, bInfo.id)
    return buffer
end

function BufferMgr:initBuffer()
    local data = BattleData.charData
    for charId, info in pairs(data) do
        local buffer = self:createBuffer(info.bufferName, charId)
        buffer:setTargetId(charId)
        buffer:setRounds(info.rounds)
    end
end

function BufferMgr:pushBuffer(buffer)
    local rounds = buffer:getRounds()
    if rounds == 0 then
        local char = InstanceMgr.getIns(InstanceMgr.CHARLAYER):getCharById(buffer:getTargetId())
        char:pushBuffer(buffer)

        local propType = buffer:getPropType()
        local value = buffer:getFinalValue()
        if propType == BufferConst.PROP_HP or propType == BufferConst.PROP_DAMAGE then
            local id = buffer:getTargetId()
            BattleData.updateHpById(id, value)
        elseif propType == BufferConst.PROP_CP then
            local id = buffer:getTargetId()
            BattleData.updateCpById(id, value)
        else
        end
    else
    end
end

function BufferMgr:getBufferById(id)
    return self.bufferList[id]
end

function BufferMgr:setBuffer(buffer)
    self.bufferList[buffer:getId()] = buffer
end

return BufferMgr
