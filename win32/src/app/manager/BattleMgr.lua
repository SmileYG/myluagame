--local BasicTable = require("app.basicObj.BasicTable")
--local BattleMgr = class("BattleMgr", BasicTable)
local BattleMgr = class("BattleMgr")
local EventMgr = require("app.manager.EventMgr")
local MapInfo = require("app.module.data.MapInfo")
local MapInfoConst = require("app.const.MapInfoConst")
local HeroInfo = require("app.config.HeroInfo")
local CharConst = require("app.const.CharConst")
local BasicObjConst = require("app.const.BasicObjConst")
local BattleUtil = require("app.util.BattleUtil")
local BattleData = require("app.module.data.BattleData")
local BattleConst = require("app.const.BattleConst")
local AttackRange = require("app.config.AttackRange")
local DelayEffectInfo = require("app.config.DelayEffectInfo")
local ProjectileInfo = require("app.config.ProjectileInfo")
local AttackTipsArrows = require("app.component.AttackTipsArrows")
local socket = require("socket")
local OtherUtil = require("app.util.OtherUtil")

local ipairs = ipairs
local pairs = pairs
local table_insert = table.insert

local firstTouchTime = 0

local DEVIATION_VALUE = 10

local REFE_SELF = 0
local REFE_TARGET = 1
local DOUBLE_TIME_INTERVAL = 0.2 -- 两次触屏的时间间隔小于这个值就当做是双击
local ACTION_TOTAL_VALUE = 5 -- 每回合内可行动的总次数

function BattleMgr:ctor()
    --BattleMgr.super.ctor(self)
    BattleMgr:init()
end

function BattleMgr:init()
    self.mapInfo = InstanceMgr.getIns(InstanceMgr.MAPINFO)
    self.charLayer = InstanceMgr.getIns(InstanceMgr.CHARLAYER)
    self.mapLayer = InstanceMgr.getIns(InstanceMgr.MAPLAYER)
    self.touchLayer = InstanceMgr.getIns(InstanceMgr.TOUCHLAYER)
    self.battleUILayer = InstanceMgr.getIns(InstanceMgr.BATTLEUILAYER)
    self.skillMgr = InstanceMgr.getIns(InstanceMgr.SKILLMGR)
    self.bufferMgr = InstanceMgr.getIns(InstanceMgr.BUFFERMGR)
    self.effectMgr = InstanceMgr.getIns(InstanceMgr.EFFECTMGR)
    self.battleEffectLayer = InstanceMgr.getIns(InstanceMgr.BATTLEEFFECTLAYER)
    self.soundMgr = InstanceMgr.getIns(InstanceMgr.SOUNDMGR)

    self.ownList = {}
    self.enemyList = {}
    self.charsList = {}
    self.beganPoint = {}
    self.touchState = 0
    self:resetActionValue()
    self:regEvent()
end

function BattleMgr:regEvent()
    BattleMgr.func_1 = handler(self, self.onResetScene)
    BattleMgr.func_2 = handler(self, self.onTouchEvent)
    BattleMgr.func_3 = handler(self, self.charDoSkill)
    BattleMgr.func_4 = handler(self, self.skillEnd)
    BattleMgr.func_5 = handler(self, self.normalAttackEventHandler)
    BattleMgr.func_6 = handler(self, self.skillAttackEventHandler)
    BattleMgr.func_7 = handler(self, self.skillMultEventHandler)
    BattleMgr.func_8 = handler(self, self.effectCallBack)
    BattleMgr.func_9 = handler(self, self.setDoingId)
    BattleMgr.func_10 = handler(self, self.unsetDoingId)

    EventMgr.regEvent(EventType.RESET_SCENE, BattleMgr.func_1)
    EventMgr.regEvent(EventType.TOUCH_EVENT, BattleMgr.func_2)
    EventMgr.regEvent(EventType.DO_SKILL, BattleMgr.func_3)
    EventMgr.regEvent(EventType.SKILL_END, BattleMgr.func_4)
    EventMgr.regEvent(EventType.NORMAL_ATTACK_EVENT, BattleMgr.func_5)
    EventMgr.regEvent(EventType.SKILL_ATTACK_EVENT, BattleMgr.func_6)
    EventMgr.regEvent(EventType.SKILL_MULT_EVENT, BattleMgr.func_7)
    EventMgr.regEvent(EventType.EFFECT_FRAME_EVENT, BattleMgr.func_8)
    EventMgr.regEvent(EventType.SET_DOING_ID, BattleMgr.func_9)
    EventMgr.regEvent(EventType.UNSET_DOING_ID, BattleMgr.func_10)
end

function BattleMgr:unRegEvent()
    EventMgr.unRegEvent(EventType.RESET_SCENE, BattleMgr.func_1)
    EventMgr.unRegEvent(EventType.TOUCH_EVENT, BattleMgr.func_2)
    EventMgr.unRegEvent(EventType.DO_SKILL, BattleMgr.func_3)
    EventMgr.unRegEvent(EventType.SKILL_END, BattleMgr.func_4)
    EventMgr.unRegEvent(EventType.NORMAL_ATTACK_EVENT, BattleMgr.func_5)
    EventMgr.unRegEvent(EventType.SKILL_ATTACK_EVENT, BattleMgr.func_6)
    EventMgr.unRegEvent(EventType.SKILL_MULT_EVENT, BattleMgr.func_7)
    EventMgr.unRegEvent(EventType.EFFECT_FRAME_EVENT, BattleMgr.func_8)
    EventMgr.unRegEvent(EventType.SET_DOING_ID, BattleMgr.func_9)
    EventMgr.unRegEvent(EventType.UNSET_DOING_ID, BattleMgr.func_10)
end

function BattleMgr:onTouchEvent(event)
    if event.name == "began" then
        --Logger.debug("began:",self.touchState)
        self.beganPoint.x = event.x
        self.beganPoint.y = event.y
        --Logger.debug("BattleMgr touch began:", event.x, event.y)
        local isSwallow = self.battleUILayer:onTouchEvent(event)
        if isSwallow then 
            self:cancelSelectedHandler()
            return 
        end
        
        if self.selectedCharId then
            if self.touchState == 0 then
                self.touchState = 1
            else
                self.touchState = 4
            end
        end

        if not self.selectedCharId then
            local gridIndex = self.mapLayer:getGridIndexByPoint(cc.p(event.x, event.y))
            self:onTouchEndedGrid(gridIndex)
        end

        --[[
        local item = self.battleUILayer:getSelectedItem()
        if item then
            self:cancelSelectedHandler()
        end
        ]]--
    elseif event.name == "moved" then
        --Logger.debug("moved:",self.touchState)
        self.battleUILayer:onTouchEvent(event)
        if not self.selectedCharId then return end
        
        if not (math.abs(self.beganPoint.x - event.x) >= DEVIATION_VALUE) and 
            not (math.abs(self.beganPoint.y - event.y) >= DEVIATION_VALUE) then
            if self.touchState ~= 4 then self.touchState = 2 end
            return
        end

        self.touchState = 4
        self:selectingTarget(event.x, event.y)
        self:selectedTarget(event.x, event.y)
    elseif event.name == "ended" then
        --Logger.debug("ended:",self.touchState)
        --Logger.debug("BattleMgr touch ended:", event.x, event.y)
        local item = self.battleUILayer:getSelectedItem()
        local gridIndex = self.mapLayer:getGridIndexByPoint(cc.p(event.x, event.y))
        local char = nil
        local id = nil
        if item and gridIndex then
            local bool = self:isInRange(self.battleUILayer:getApplyRange(), gridIndex)
            if bool then
                id = self.mapInfo:getCharIdFromGrid(gridIndex)
                char = self.charLayer:getCharById(id)
                if char then
                    self:addItemBuffer(item, char)
                    return
                end
            end
            self.battleUILayer:onTouchEvent(event)
        elseif item then
            self.battleUILayer:onTouchEvent(event)
        elseif gridIndex then
            if not self.selectedCharId then return end

            if self.touchState ~= 4 then 
                self.touchState = 3 
                return
            end

            if self.touchState == 4 then
                self.touchState = 0
                if gridIndex then
                    self.mapInfo:getCharIdFromGrid(gridIndex)
                    char = self.charLayer:getCharById(id)
                    if not char or char:getId() ~= self.selectedCharId then
                        self:onTouchEndedGrid(gridIndex)
                        return
                    end
                end
                self:cancelSelectedHandler()
                return
            end
        end

        
        
        -----------------------------------------------------------------------------------
        --[[
        do return end
        local item = self.battleUILayer:getSelectedItem()
        local gridIndex = self.mapLayer:getGridIndexByPoint(cc.p(event.x, event.y))
        if item and gridIndex then
            local bool = self:isInRange(self.battleUILayer:getApplyRange(), gridIndex)
            if bool then 
                local id = self.mapInfo:getCharIdFromGrid(gridIndex)
                local char = self.charLayer:getCharById(id)
                if char then
                    self:createItemBuffer(item, char)
                    return
                end
            end
            self.battleUILayer:onTouchEvent(event)
        elseif item then
            self.battleUILayer:onTouchEvent(event)
        elseif gridIndex then
            local isSwallow = self.battleUILayer:onTouchEvent(event)
            --if isSwallow then return end
            --self:cancelSelectedHandler()
            self:onTouchEndedGrid(gridIndex)
        else
            self.battleUILayer:onTouchEvent(event)
        end
        ]]--
    end
end

function BattleMgr:selectingTarget(x, y)
    if not self.selectedCharId then return end 

    local char = self.charLayer:getCharById(self.selectedCharId)
    local fromPos = BattleUtil.getBoneWorldPositionByName(BasicObjConst.POINT_SIGN, char)
    if self.tipsArrows then
        local toPos = cc.p(x, y)
        local angle = BattleUtil.calculateProjtAngle(fromPos, toPos)
        self.tipsArrows:setForm(angle, toPos)
    else
        self:createAttackTipsArrows(fromPos)
    end
end

-- 显示可被攻击的标志
function BattleMgr:selectedTarget(x, y)
    if not self.selectedCharId then 
        Logger.debug("no self.selectedCharId, return")
        return 
    end 

    local gridIndex = self.mapLayer:getGridIndexByPoint(cc.p(x, y))
    if not gridIndex then 
        Logger.debug("not gridIndex, return")
        return 
    end

    local id = self.mapInfo:getCharIdFromGrid(gridIndex)
    if self.selectedTargetId and id and self.selectedTargetId == id then
        Logger.debug("self.selectedTargetId and id are equal, return:", self.selectedTargetId, id)
        return
    end

    local list = self.mapInfo:getEnemysGridIndex()
    local bool = OtherUtil.isExistGridIndexInList(list, gridIndex)
    if not bool then 
        Logger.debug("not exist in waiting enemysGridIndexList")
        local attacker = self.charLayer:getCharById(self.selectedCharId)
        local attackMode = attacker:getAttackMode()
        if attackMode == CharConst.ATTACK_MODE_NORMAL then
            if not self.selectedTargetId then return end
            local target = self.charLayer:getCharById(self.selectedTargetId)
            target:removeCurSign()
        elseif attackMode == CharConst.ATTACK_MODE_SKILL then
            local list = self.mapInfo:getSkillEnemysGridIndex()
            for _, _gridIndex in pairs(list) do
                local _id = self.mapInfo:getCharIdFromGrid(_gridIndex)
                local char = self.charLayer:getCharById(_id)
                char:removeCurSign()
            end
            self.mapInfo:resetSkillEnemysGridIndex()
        end
        self.selectedTargetId = nil
        return
    end

    if id and self.selectedCharId == id then 
        Logger.debug("self.selectedCharId and id are equal, return")
        return
    end

    local attacker = self.charLayer:getCharById(self.selectedCharId)
    local attackMode = attacker:getAttackMode()
    self.selectedTargetId = id
    if attackMode == CharConst.ATTACK_MODE_NORMAL then
        local target = self.charLayer:getCharById(id)
        target:showSign(CharConst.CAN_BE_ATTACKED)
    elseif attackMode == CharConst.ATTACK_MODE_SKILL then
        local enemysGridIndexList = self.mapInfo:searchSkillTargetsGridIndex(gridIndex, attacker:getHeroId())
        for _, _gridIndex in pairs(enemysGridIndexList) do
            local _id = self.mapInfo:getCharIdFromGrid(_gridIndex)
            local char = self.charLayer:getCharById(_id)
            char:showSign(CharConst.CAN_BE_ATTACKED)
        end
    end
end

function BattleMgr:createAttackTipsArrows(pos)
    self.tipsArrows = AttackTipsArrows.new(pos)
end

function BattleMgr:removeAttackTipsArrows()
    if not self.tipsArrows then return end
    self.tipsArrows:destory()
    self.tipsArrows = nil
end

function BattleMgr:onTouchEndedGrid(gridIndex)
    if not gridIndex then return end

    if self:isDoubleTouch() then
        Logger.debug("double..................")
        return
    end

    --self:hideHpBar()
    local areaType = self.mapInfo:getAreaTypeByGridIndex(gridIndex)
    if not self.selectedCharId then
        if areaType == MapInfoConst.FORCE_OWN then
            self:selectedHandler(gridIndex) 
        end
    else
        if areaType == MapInfoConst.FORCE_OWN then
            local id = self.mapInfo:getCharIdFromGrid(gridIndex)
            local char = self.charLayer:getCharById(id)
            if char:getId() == self.selectedCharId 
                or not char:canHelpOthers() then
                --self.selectedChar:doRestore()
                --self.clearAllTargetsPos()
                --self.selectedChar = nil
            end
            self:cancelSelectedHandler()
        elseif areaType == MapInfoConst.FORCE_ENEMY then
            self:charAttackHandler(gridIndex)
            --[[
            callBack = function()
                self:charDoAttackHandler(gridIndex)
            end
            self:setAttackerId(self.selectedCharId)
            self:cancelSelectedHandler(callBack)
            ]]--
        elseif areaType == MapInfoConst.FORCE_NULL then
            self:charMoveHandler(gridIndex)
        end
    end
end

function BattleMgr:selectedHandler(gridIndex)
    local id = self.mapInfo:getCharIdFromGrid(gridIndex)
    if self:isDoingId(id) then return end

    self.selectedSoundId = self.soundMgr:playMusic(2)
    self.soundMgr:playSoundEffect(3)

    self.charsList = {}
    local char = self.charLayer:getCharById(id)
    local heroId = char:getHeroId()
    local attackType = char:getAttackType()
    local attackMode = char:getAttackMode()
    local profession = char:getProfession()
    char:doMagnify()

    local pathInfo = self.mapInfo:searchMovePath(heroId, gridIndex)
    local enemysGridIndex, filteredTargetsGridIndex, ownsGridIndex = self.mapInfo:searchTargetsGridIndex(heroId, gridIndex, attackMode)

    self.mapLayer:showMoveGrid(pathInfo)
    --char:setPathInfo(pathInfo)
    --char:setAttackTargetsGridIndex(targetsGridIndex)
    self.selectedCharId = id

    if table.nums(enemysGridIndex) ~= 0 then
        for _, _gridIndex in pairs(enemysGridIndex) do
            local id = self.mapInfo:getCharIdFromGrid(_gridIndex)
            local _char = self.charLayer:getCharById(id)
            if not _char:isDeath() then
                _char:showSign(CharConst.WAITING_SELECT)
                table_insert(self.charsList, _char)
            end
        end
    end

    if attackType == BattleConst.ATTACK_TYPE_BLOCKED then
        for _, _gridIndex in pairs(filteredTargetsGridIndex) do
            local id = self.mapInfo:getCharIdFromGrid(_gridIndex)
            local _char = self.charLayer:getCharById(id)
            if not _char:isDeath() then
                _char:showSign(CharConst.CAN_NOT_BE_SELECTED)
                table_insert(self.charsList, _char)
            end
        end
    end

    if profession == CharConst.HERO_TYPE_DOCTOR then
        for _, _gridIndex in pairs(ownsGridIndex) do
            local id = self.mapInfo:getCharIdFromGrid(_gridIndex)
            local _char = self.charLayer:getCharById(id)
            _char:showSign(CharConst.CAN_BE_RESCUED)
            table_insert(self.charsList, _char)
        end
        --[[
        local targetsGridIndex = self.mapInfo:searchHelpTargetsGridIndex(heroId, gridIndex)
        if table.nums(targetsGridIndex) ~= 0 then
            char:setHelpTargetsGridIndex(targetsGridIndex)
            for _, _gridIndex in pairs (targetsGridIndex) do
                local _char = self.charLayer:getCharById(_gridIndex)
                _char:showSign(CharConst.CAN_BE_RESCUED)
                table_insert(self.charsList, _char)
            end
        end
        ]]--
    end
end

-- 取消选中状态
function BattleMgr:cancelSelectedHandler(callBack)
    if self.selectedSoundId then
        self.soundMgr:stopById(self.selectedSoundId)
        self.soundMgr:playSoundEffect(4)
        self.selectedSoundId = nil
    end

    self:removeAttackTipsArrows()
    for _, char in ipairs(self.charsList) do
        char:removeAllSign()
    end
    
    if not self.selectedCharId then return end 
    local char = self.charLayer:getCharById(self.selectedCharId)
    char:doRestore(callBack)
    self.selectedCharId = nil
    self.mapLayer:hideMoveGrid()
end

-- char移动操作
function BattleMgr:charMoveHandler(desGridIndex)
    local charId = self.selectedCharId
    local origGridIndex = BattleData.getGridIndexById(charId)
    self.mapInfo:setCharIdToGrid(charId, origGridIndex, desGridIndex)
    BattleData.setGridIndexById(charId, desGridIndex)
    local callBack = function()
        self:charMove(charId, origGridIndex, desGridIndex)
    end
    self:cancelSelectedHandler(callBack)
end

function BattleMgr:charMove(charId, origGridIndex, desGridIndex)
    if not self.mapInfo:canMoveByGridIndex(desGridIndex) then
        Logger.debug("不能移动到此位置")
        return 
    end

    self:updateActionValue(-1)
    --self:setTouchEnabled(false)
    local char = self.charLayer:getCharById(charId)
    --local origGridIndex = char:getGridIndex()
    local needTurnBack = BattleUtil.isNeedTurnBack(origGridIndex.x, desGridIndex.x, char:getInitDir())
    if needTurnBack then char:turnBack() end

    local desPosition = BattleUtil.getDestinationPosition(desGridIndex, char)
    local actions = {}
    local action = cc.MoveTo:create(BasicObjConst.MOVE_TIME, cc.p(desPosition.x, desPosition.y))
    actions[#actions + 1] = action
    
    local func = cc.CallFunc:create(function()
        char:doStand()
    end)
    actions[#actions + 1] = func 

    local _action = transition.sequence(actions)
    char:runAction(_action)
    char:doMove()
    
    --self.mapInfo:setAreaTypeByGridIndex(origGridIndex, MapInfoConst.FORCE_NULL)
    --self.mapInfo:setAreaTypeByGridIndex(gridIndex, MapInfoConst.FORCE_OWN)
    --char:setGridIndex(gridIndex)
end

function BattleMgr:charAttackHandler(gridIndex)
    local callBack = function()
        self:charDoAttackHandler(gridIndex)
    end
    self:setAttackerId(self.selectedCharId)
    self:cancelSelectedHandler(callBack)
end

function BattleMgr:charDoAttackHandler(gridIndex)
    if not self.mapInfo:canAttackByGridIndex(gridIndex) then
        Logger.debug("不能攻击此位置")
        return
    end

    self:updateActionValue(-1)
    --self:setTouchEnabled(false)
    local attacker = self.charLayer:getCharById(self.attackerId)
    local attackMode = attacker:getAttackMode()
    
    Logger.debug("charDoAttackHandler:",attackMode)
    if attackMode == CharConst.ATTACK_MODE_NORMAL then
        self:charDoNormalAttack(gridIndex)
    else
        self:charDoSkillAttack(gridIndex)
    end
end

function BattleMgr:charDoNormalAttack(gridIndex)
    local attacker = self.charLayer:getCharById(self.attackerId)
    local attackMode = attacker:getAttackMode()
    local id = self.mapInfo:getCharIdFromGrid(gridIndex)
    self.attackTarget = self.charLayer:getCharById(id)
    self:findTargets(gridIndex, attackMode)
    self:pushBuffer(attackMode)

    local origGridIndex = attacker:getGridIndex()
    local needTurnBack = BattleUtil.isNeedTurnBack(origGridIndex.x, gridIndex.x, attacker:getInitDir())
    if needTurnBack then attacker:turnBack() end
    attacker:doAttack()
    --attacker:doSkill()
end

-- 以触摸格子找目标
function BattleMgr:findTargets(gridIndex, attackMode)
    --[[
    self.targets = {}
    local attacker = self.charLayer:getCharById(self.attackerId)
    local rangeType = nil
    if attackMode == CharConst.ATTACK_MODE_NORMAL then
        rangeType = attacker:getNRangeType()
    else
        rangeType = attacker:getSRangeType()
    end

    local info = AttackRange[rangeType]
    ]]--
    --暂时注释掉这里
    --if info.center == REFE_SELF then
        --gridIndexList = self.mapInfo:findTargets(info.range, attacker:getGridIndex(), gridIndex)
    --elseif info.center == REFE_TARGET then
        --gridIndexList = self.mapInfo:findTargets(info.range, gridIndex)
    --end

    local gridIndexList = self.mapInfo:getEnemysGridIndex()
    local n = 0
    self.targets = {}
    for i, gridIndex in pairs(gridIndexList) do
        local id = self.mapInfo:getCharIdFromGrid(gridIndex)
        local char = self.charLayer:getCharById(id)
        if not char:isDeath() then
            n = n + 1
            self.targets[n] = char 
        end
    end
end

function BattleMgr:pushBuffer(attackMode)
    local attacker = self.charLayer:getCharById(self.attackerId)
    local info 
    if attackMode == CharConst.ATTACK_MODE_NORMAL then
        info = attacker:getAttackInfo()
    else
        info = attacker:getSkillInfo()
    end
    local selfBufferIdList = info["self_buffer"]
    local targetBufferIdList = info["target_buffer"]

    if targetBufferIdList then
        for _, bName in ipairs(targetBufferIdList) do
            for _, target in ipairs(self.targets) do
                local buffer = self.bufferMgr:createBuffer(bName, target:getId())
                buffer:setReId(attacker:getId())
                buffer:setTargetId(target:getId())
                --BattleData.setBufferIdById(target:getId(), buffer:getId())
                self.bufferMgr:pushBuffer(buffer)
                --target:pushBuffer(buffer)
            end
        end
    end

    if selfBufferIdList then
        for _, bName in ipairs(selfBufferIdList) do
            local buffer = self.bufferMgr:createBuffer(bName, attacker:getId())
            buffer:setTargetId(attacker:getId())
            self.bufferMgr:pushBuffer(buffer)
            --BattleData.setBufferIdById(target:getId(), buffer:getId())
            --attacker:pushBuffer(buffer)
        end
    end
end

function BattleMgr:normalAttackEventHandler()
    local attacker = self.charLayer:getCharById(self.attackerId)
    local info = attacker:getAttackInfo()
    self:checkAction(info)
end

function BattleMgr:charDoHitHanlder()
    for _, target in ipairs(self.targets) do
        target:doHitHandler()
    end
    self:stageEnd()
end

function BattleMgr:charDoSkill()
    local attacker = self.charLayer:getCharById(self.attackerId)
    attacker:doSkill()
end

function BattleMgr:charDoSkillAttack(gridIndex)
    local attacker = self.charLayer:getCharById(self.attackerId)
    local attackMode = attacker:getAttackMode()
    local skillId = attacker:getSkillInfo().skillId
    self:findTargets(gridIndex, attackMode)
    self:pushBuffer(attackMode)

    local info = {
        ["skillId"] = skillId,
        ["gridIndex"] = gridIndex,
        ["attacker"] = attacker,
        ["targets"] = self.targets
    }
    self.skillMgr:skillStart(info)
end

function BattleMgr:skillAttackEventHandler()
    local attacker = self.charLayer:getCharById(self.attackerId)
    local info = attacker:getSkillInfo()
    self:checkAction(info)
    --self:trigSkillExecuteEvent()
end

function BattleMgr:skillMultEventHandler()
    for _, target in ipairs(self.targets) do
        target:doHit()
    end
end

--[[
function BattleMgr:trigSkillExecuteEvent()
    EventMgr.trigEvent(EventType.SKILL_EXECUTE_EVENT)
end
]]--

--[[
function BattleMgr:addSkillEffect()
    local attacker = self.charLayer:getCharById(self.attackerId)
    local info = attacker:getSkillInfo()
    self:checkAction(info)
end
]]--

function BattleMgr:addEffect(info)
    local isProjectile = string.find(info.effectId, "projt")
    local attacker = self.charLayer:getCharById(self.attackerId)
    local castPointName = info.castPointName
    local destPointName = info.destPointName
    local parent = nil
    if info.mountParent == 0 then
        parent = self.battleEffectLayer
    elseif info.mountParent == 1 then
        parent = attacker
    end

    if isProjectile then
        -- 投掷飞行类特效
        -- 有位移的飞行道具或特效的parent基本都是battleEffectLayer
        -- 暂时注释掉
        local isFixedCastPoint = toBooleanEx(info.fixedCastPoint)
        local fromPos = nil
        local toPos = nil
        local angle = nil
        self.effectsList = {}

        if not isFixedCastPoint then
            fromPos = BattleUtil.getBoneWorldPositionByName(castPointName, attacker)
            for _, target in ipairs(self.targets) do
                toPos = BattleUtil.getBoneWorldPositionByName("hitPoint", target)
                angle = BattleUtil.calculateProjtAngle(fromPos, toPos)

                local effect = self:addProjectile(info.effectId, BattleConst.NORMAL_EFFECT, parent, fromPos)
                transition.execute(effect, cc.MoveTo:create(0.1, toPos), {
                    onComplete = function()
                        effect:removeFromParent()
                        self:effectActionEndCallback(target)
                    end
                })

                effect:setRotation(angle)
                table_insert(self.effectsList, effect)
                self.callbackNums = self.callbackNums + 1
            end
        else
            --释放点固定位置
            for _, target in ipairs(self.targets) do
                local zorder = target:getLocalZOrder()
                toPos = BattleUtil.getBoneWorldPositionByName(destPointName, target)
                fromPos, angle = BattleUtil.getReleasePoint(toPos)
                local effect = self:addProjectile(info.effectId, BattleConst.NORMAL_EFFECT, parent, fromPos)
                local time = math.random(0.7, 1)
                transition.execute(effect, cc.MoveTo:create(time, toPos), {
                    onComplete = function()
                        effect:removeFromParent()
                        self:effectActionEndCallback(target)
                    end
                })

                effect:setLocalZOrder(zorder)
                effect:setRotation(angle)
                table_insert(self.effectsList, effect)
                self.callbackNums = self.callbackNums + 1
            end
        end
    else
        if toBooleanEx(info.multiEffect) then
            self.isMulitEffect = true
            for _, target in ipairs(self.targets) do
                self.callbackNums = self.callbackNums + 1
                parent = target
                local destPoint = parent:getBoneNodePositionByName(destPointName)
                local effect =  self:playBattleEffect(info.effectId, BattleConst.EVENT_EFFECT, parent, destPoint)
            end
        else
            self.callbackNums = 1
            self.isMulitEffect = false
            -- 纯序列帧
            self = self:playBattleEffect(info.effectId, BattleConst.EVENT_EFFECT, parent)
            
        end
    end
end

function BattleMgr:addProjectile(effectId, _type, parent, fromPoint)
    local isDelayEffect = string.find(effectId, "de")
    local projt = nil
    if not isDelayEffect then
        projt = self:createProjectile(effectId, parent, fromPoint)
    else
        projt = self:playBattleEffect(effectId, _type, parent, fromPoint)
        projt = projt:getAnimation()
    end
    return projt
end

function BattleMgr:createProjectile(effectId, parent, fromPoint)
    local res = ProjectileInfo[effectId]["res"]
    local projt = display.newSprite("projectile/" .. res)
    parent = self.battleEffectLayer
    parent:addChild(projt)
    projt:setPosition(fromPoint)
    return projt
end

function BattleMgr:playBattleEffect(effectId, _type, parent, point)
    local effect = self.effectMgr:createEffect(effectId, _type, parent, point)
    effect:play()
    return effect
end

function BattleMgr:effectActionEndCallback(target)
    self.callbackNums = self.callbackNums - 1
    target:doHitHandler()
    Logger.debug("effectActionEndCallback...:",self.callbackNums)
    if self.callbackNums == 0 then
        self:stageEnd()
    end
end

function BattleMgr:effectCallBack(target)
    if self.isMulitEffect then
        self:effectActionEndCallback(target)
    else
        self.callbackNums = self.callbackNums - 1
        for _, target in ipairs(self.targets) do
            target:doHit()
        end

        if self.callbackNums == 0 then
            self:stageEnd()
        end
    end
    --EventMgr.trigEvent(EventType.ATTACK_HIT)
end

function BattleMgr:checkAttackerBuffer()
    local attacker = self.charLayer:getCharById(self.attackerId)
    attacker:pullBuffer()
end

function BattleMgr:stageEnd()
    self:checkAttackerBuffer()
    local attacker = self.charLayer:getCharById(self.attackerId)
    local attackMode = attacker:getAttackMode()
    if attackMode == CharConst.ATTACK_MODE_NORMAL then
        self:normalAttackEnd()
    else
        attacker:clearCp()
        EventMgr.trigEvent(EventType.SKILL_EXECUTE_EVENT)
    end
end

function BattleMgr:checkAction(info)
    self.callbackNums = 0
    if info.effectId then
        self:addEffect(info)
    else
        self:charDoHitHanlder()
    end
end

function BattleMgr:normalAttackEnd()
    Logger.debug("普通攻击结束@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    --self:setTouchEnabled(true)
end

function BattleMgr:skillEnd()
    Logger.debug("技能结束阶段@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    --self:setTouchEnabled(true)
end

function BattleMgr:addItemBuffer(item, char)
    local bufferInfo = item:getBufferInfo()
    self.battleUILayer:removeItem(item:getId())
    for _, bName in ipairs(bufferInfo) do
        local buffer = self.bufferMgr:createBuffer(bName, char:getId())
        buffer:setTargetId(char:getId())
        --char:playBufferEffect(buffer)
        self.bufferMgr:pushBuffer(buffer)
        --char:addBuffer(buffer)
    end
end

function BattleMgr:updateActionValue(value)
    self.actionCurValue = self.actionCurValue + value
    if self.actionCurValue == 0 then
        Logger.debug("可行动的次数已经用完。。。。。。。。。。。。。")
    end
end

function BattleMgr:checkActionValue()
    return toBooleanEx(self.actionCurValue)
end

function BattleMgr:resetActionValue()
    self.actionCurValue = ACTION_TOTAL_VALUE
end

function BattleMgr:showHpBar()
    local chars = self.charLayer:getAllChars()
    for _, char in pairs (chars) do
        char:setHpBarVisible(true)
    end
end

function BattleMgr:hideHpBar()
    local chars = self.charLayer:getAllChars()
    for _, char in pairs (chars) do
        char:setHpBarVisible(false)
    end
end

function BattleMgr:isDoubleTouch()
    local touchTime = socket.gettime()
    local timeDiff = touchTime - firstTouchTime
    if timeDiff <= DOUBLE_TIME_INTERVAL then
        firstTouchTime = 0
        return true
    end
    firstTouchTime = touchTime
    return false
end

function BattleMgr:isInRange(ranges, gridIndex)
    for _, info in pairs(ranges) do
        if info.gridIndex.x == gridIndex.x and
            info.gridIndex.y == gridIndex.y then
            return true
        end
    end
    return false
end

function BattleMgr:isDoingId(id)
    if self.doingId and self.doingId == id then return true end
    return false
end

function BattleMgr:getDoingId()
    return self.doingId
end

function BattleMgr:setDoingId(id)
    self.doingId = id
end

function BattleMgr:unsetDoingId(id)
    if self.doingId and self.doingId == id then self.doingId = nil end
end

--[[
function BattleMgr:setTouchEnabled(bool)
    self.touchLayer:setTouch(bool)
end
]]--

function BattleMgr:setAttackerId(charId)
    self.attackerId = charId
end

function BattleMgr:setMoverId(charId)
    self.moverId = charId
end

function BattleMgr:onResetScene()
    self:unRegEvent()
end

return BattleMgr
