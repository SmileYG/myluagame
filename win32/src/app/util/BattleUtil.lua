local BattleUtil = {}
local BasicObjConst = require("app.const.BasicObjConst")
local CharConst = require("app.const.CharConst")
local BufferConst = require("app.const.BufferConst")

local ipairs = ipairs
local pairs = pairs
local math_max = math.max

-- 计算两个关键点的像素距离
function BattleUtil.getDestinationPosition(gridIndex, char)
    local mapLayer = InstanceMgr.getIns(InstanceMgr.MAPLAYER)
    local standPosToWorld = mapLayer:getStandPosToWorldSpace(gridIndex.x, gridIndex.y)
    local bonePosition = char:getBoneNodePositionByName(BasicObjConst.POINT_STAND)
    local dir = char:getDir()
    local desPosition = cc.p(standPosToWorld.x - (bonePosition.x * dir), standPosToWorld.y - bonePosition.y)
    return desPosition
end

function BattleUtil.getDistanceByPoint(fromPoint, toPoint)
    local a = math.pow((fromPoint.x - toPoint.x), 2)
    local b = math.pow((fromPoint.y - toPoint.y), 2)
    return math.sqrt(a + b)
end

function BattleUtil.getBoneWorldPositionByName(pointName, char)
    local position = char:getBoneNodePositionByName(pointName)
    local mapLayer = InstanceMgr.getIns(InstanceMgr.MAPLAYER)
    return char:convertToWorldSpace(position)
end

function BattleUtil.getReleasePoint(targetPoint)
    -- 写死为了快速实现效果
    local p = cc.p(0, 0)
    if targetPoint.x > display.cx then
        --p.x = math.random(100, 200)
        p.x = 150
        --p.y = math.random(display.height + math.floor(targetPoint.y / 100) * 10, display.height + 100)
        p.y = display.height + (targetPoint.y / 100) * 10
    else
        --p.x = math.random(display.width - 200, display.width - 100)
        p.x = display.width - 150
        --p.y = math.random(display.height + math.floor(targetPoint.y / 100) * 10, display.height + 100)
        p.y = display.height + (targetPoint.y / 100) * 10
    end

    local angle = BattleUtil.calculateAngle(p, targetPoint)

    return p, angle
end

-- 是否需要转身
function BattleUtil.isNeedTurnBack(origX, desX, initDir)
    print("aaa:",origX,desX,initDir)
    local value = (desX - origX) * initDir
    if value >= 0 then
        return false
    else
        return true
    end
end

-- 根据攻击范围计算距离格子数
function BattleUtil.calculateAttackDistance(attackRange)
    local value = nil
    for i, tbl in ipairs(attackRange) do
        if i == 1 then
            value = tbl.x + tbl.y
        else
            value = math_max(value, tbl.x + tbl.y)
        end
    end
    return value
end

-- 两个点计算角度
function BattleUtil.calculateAngle(startPoint, desPoint)
    local _x = startPoint.x - desPoint.x
    local _y = startPoint.y - desPoint.y
    local angle = (180 * math.atan(_x / _y)) / math.pi
    return angle
end

-- 计算投掷物的角度
-- startPt:起始点  desPt:目标点
function BattleUtil.calculateProjtAngle(startPt, desPt)
    local _x = startPt.x - desPt.x
    local _y = desPt.y - startPt.y
    local angle = (180 * math.atan(_y / _x)) / math.pi
    if startPt.x > desPt.x then angle = angle + 180 end
    return  angle
end

-- 攻击者单方面的伤害值
function BattleUtil.getCharDamage(charId)
    local char = BattleUtil.getChar(charId)
    if char:getBA(CharConst.PROP_STR) then
        char:getFA(BufferConst.TYPE_STR)
    else
        char:getFA(BufferConst.TYPE_ATS)
    end
end

-- char基本属性
function BattleUtil.getCharBA(charId, _type)
    local char = BattleUtil.getChar(charId)
    return char:getBA(_type)
end

-- char基本属性+其他加成
function BattleUtil.getCharFA(charId, _type)
    local char = BattleUtil.getChar(charId)
    return char:getFA(_type)
end

function BattleUtil.getChar(charId)
    local charLayer = InstanceMgr.getIns(InstanceMgr.CHARLAYER)
    local char = charLayer:getCharById(charId)
    return char
end

return BattleUtil
