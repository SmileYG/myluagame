local OtherUtil = {}

function cloneEx(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" or object.canNotClone then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for key, value in pairs(object) do
            new_table[_copy(key)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end

-- 除了0和nil, 其他都转为true
function toBooleanEx(value)
    if value and value ~= 0 then
        return true
    end
    return false
end

-- 参数gridIndex是否存在于gridIndexList中
function OtherUtil.isExistGridIndexInList(list, gridIndex)
    for _, _gridIndex in pairs(list) do
        if _gridIndex.x == gridIndex.x and _gridIndex.y == gridIndex.y then return true end
    end
    return false
end

return OtherUtil
