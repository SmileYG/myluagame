Logger = {}
Logger.DEBUG    = 1
Logger.INFO     = 2
Logger.WARN     = 3
Logger.ERROR    = 4
Logger.FATAL    = 5

Logger.StartInfo = {
    "[GAME_DEBUG]:",
    "[GAME_INFO]:",
    "[GAME_WARN]:",
    "[GAME_ERROR]:",
    "[GAME_FATAL]:"
}

local _print    = print
--print           = function() end

logger = Logger -- 为了兼容原来的习惯

if not LOG_LEVEL then
    LOG_LEVEL   = 1
end

local function echo(logLevel, ...) 
    if logLevel < LOG_LEVEL then
        return
    end
    _print(Logger.StartInfo[logLevel], ...) 
end

function Logger.fatal(...)
    echo(Logger.FATAL, ...) 
end

function Logger.error(...)
    echo(Logger.ERROR, ...)
end

function Logger.warn(...)
    echo(Logger.WARN, ...)
end

function Logger.info(...)
    echo (Logger.INFO, ...)
end

function Logger.debug(...)
    echo(Logger.DEBUG, ...)
end

cclog = Logger.info
