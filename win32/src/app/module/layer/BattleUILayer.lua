local BaseLayer = require("app.module.layer.BaseLayer")
local BattleUILayer = class("BattleUILayer", BaseLayer)
local BattleItem = require("app.basicObj.BattleItem")
local BattleData = require("app.module.data.BattleData")
local EventMgr = require("app.manager.EventMgr")

local ITEM_TOTAL_AMOUNT = 6
local TILERACK_RES = "ui/menuUI/TileRack-hd.png"
local ACTIONPOINTS_RES = "ui/menuUI/ActionPoints_MetalBG-hd.png"

function BattleUILayer:ctor()
    BattleUILayer.super.ctor(self)
    self.itemNo = 0
    self.itemSpaceList = {}
    self.uiSpaceList = {}
    self.mapLayer = InstanceMgr.getIns(InstanceMgr.MAPLAYER)

    self:initUI()
    self:allocateItemSpace()
    self:initItems()
end

function BattleUILayer:initUI()
    self.tileRack = display.newSprite(TILERACK_RES)
    self.tileRack:setAnchorPoint(0.5, 0)
    self.tileRack:setPosition(display.cx, 0)
    self:addChild(self.tileRack)

    self.actionPoints = display.newSprite(ACTIONPOINTS_RES)
    self.actionPoints:setAnchorPoint(0, 0)
    self.actionPoints:setPosition(0, 0)
    self:addChild(self.actionPoints)

    self:allocateUISpace(self.actionPoints, self.resetScene)
end

function BattleUILayer:onTouchEvent(event)
    if event.name == "began" then
        --print("BattleUILayer touch began:", event.x, event.y)
        local isSwallow = self:onTouchBegan(event.x, event.y)
        return isSwallow
    elseif event.name == "moved" then
        --print("BattleUILayer touch moved:", event.x, event.y)
        self:onTouchMoved(event.x, event.y)
    elseif event.name == "ended" then
        --self:setTouchSwallowEnabled(false)
        --print("BattleUILayer touch ended:", event.x, event.y)
        local isSwallow = self:onTouchEnded(event.x, event.y)
        --return isSwallow
    end
end

function BattleUILayer:onTouchBegan(x, y)
    if self.selectedItem then 
        self.canTouchEnded = true
        return true
    end

    self.canTouchEnded = false
    self.selectedItem = self:getItemByPoint(cc.p(x, y))
    if not self.selectedItem then return false end

    self.selectedItem:doMagnify()
    local mapInfo = InstanceMgr.getIns(InstanceMgr.MAPINFO)
    self.applyRanges = mapInfo:getItemApplyRange(self.selectedItem:getTargetType())
    self.mapLayer:showMoveGrid(self.applyRanges)
    return true
end

function BattleUILayer:onTouchMoved(x, y)
    if not self.selectedItem then return end

    self.canTouchEnded = true
    self.selectedItem:movingHandler(x, y)
end

function BattleUILayer:onTouchEnded(x, y)
    local bool, func = self:checkUICollision(x, y)
    if bool then
        if func then func(self) end
        --点击部分按钮需要调用下面的方法,重置按钮不需要
        --self:cancelSelectedItem()
        return true
    end

    if not self.canTouchEnded then return end
    self:cancelSelectedItem()
end

function BattleUILayer:cancelSelectedItem()
    if self.selectedItem then 
        self.selectedItem:doRestore()
        self.selectedItem = nil
    end
    self.applyRanges = nil
    self.mapLayer:hideMoveGrid()
end

function BattleUILayer:resetScene()
    EventMgr.trigEvent(EventType.RESET_SCENE)
    print("kkkkkkkkkkkkkkkkkkkkkkkkkk")
end

-- 检查是否触摸到UI
function BattleUILayer:checkUICollision(x, y)
    local point = cc.p(x, y)
    for _, tbl in ipairs(self.uiSpaceList) do
        if cc.rectContainsPoint(tbl.rect, point) then
            return true, tbl.func
        end
    end
end

-- UI的Rect存进table
function BattleUILayer:allocateUISpace(ui, func)
    local size = ui:getTextureRect()
    local rect = cc.rect(ui:getPositionX(), ui:getPositionY(), size.width, size.height)
    table.insert(self.uiSpaceList, {["rect"] = rect, ["func"] = func})
end

-- item的Rect存进table
function BattleUILayer:allocateItemSpace()
    local size = self.tileRack:getContentSize()
    local width = size.width / 6
    local p_x_1 = self.tileRack:getPositionX() - size.width / 2
    for i = 1, ITEM_TOTAL_AMOUNT do
        local x = p_x_1 + width * (i - 1)
        local rect = cc.rect(x, 0, width, size.height)
        self.itemSpaceList[i] = {["rect"] = rect, ["item"] = nil}
    end
end

function BattleUILayer:initItems()
    local itemData = BattleData.getItemData()
    for i, id in ipairs(itemData) do
        self:addItem(id)
    end
end

function BattleUILayer:addItem(id)
    self.itemNo = self.itemNo + 1
    local item = BattleItem.new(id)
    item:setId(self.itemNo)
    self:addChild(item)
    
    local x = 0
    local y = 0
    for index, tbl in ipairs(self.itemSpaceList) do
        if not tbl.item then
            tbl.item = item
            x = tbl.rect.x + tbl.rect.width / 2
            y = tbl.rect.y + 10 + tbl.rect.height / 2
            item:setPosition(x, y)
            item:setPositionOnTileRack(x, y)
            break
        end
    end
end

function BattleUILayer:removeItem(id)
    self.applyRanges = nil
    self.mapLayer:hideMoveGrid()

    if self.selectedItem:getId() == id then
        self.selectedItem = nil
    end

    for index, tbl in ipairs(self.itemSpaceList) do
        if tbl.item and tbl.item:getId() == id then
            tbl.item:removeFromParent()
            self.itemSpaceList[index]["item"] = nil
            return
        end
    end
end

function BattleUILayer:getItemByPoint(point)
    for _, tbl in ipairs(self.itemSpaceList) do
        if cc.rectContainsPoint(tbl.rect, point) then
            return tbl.item
        end
    end
end

function BattleUILayer:getApplyRange()
    return self.applyRanges
end

function BattleUILayer:isSelectedItem()
    if self.selectedItem then return true end
    return false
end

function BattleUILayer:getSelectedItem()
    return self.selectedItem
end

return BattleUILayer
