local BaseLayer = class("BaseLayer", function()
    return display.newLayer()
end)
local EventMgr = require("app.manager.EventMgr")

function BaseLayer:ctor()
    self.eventFunc_1 = handler(self, self.onResetScene)
    EventMgr.regEvent(EventType.RESET_SCENE, self.eventFunc_1)
end

function BaseLayer:onResetScene()
    EventMgr.unRegEvent(EventType.RESET_SCENE, self.eventFunc_1)
    self:removeFromParent()
end

return BaseLayer
