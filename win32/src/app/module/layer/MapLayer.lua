local Grid = require("app.basicObj.Grid")
local MapInfo = require("app.module.data.MapInfo") 
local MapInfoConst = require("app.const.MapInfoConst")
local EventMgr = require("app.manager.EventMgr")
local BaseLayer = require("app.module.layer.BaseLayer")

local MapLayer = class("MapLayer", BaseLayer)

local GRID_HD_WIDTH = 106
local GRID_HD_HEIGHT = 105
local grids_pool = {}

local ipairs = ipairs
local pairs = pairs
local table_insert = table.insert
local math_floor = math.floor

function MapLayer:ctor()
    MapLayer.super.ctor(self)
    self.mapInfo = InstanceMgr.getIns(InstanceMgr.MAPINFO)
    self.grid_visible = {}
    self:initMoveGrid()

    self.rectList = {}
    self:allocateRect()

    -- 站位点偏移量
    local offX = math_floor(GRID_HD_WIDTH / 2)
    local offY = math_floor(GRID_HD_HEIGHT / 2 * 0.5)
    self.standOffset = {x = offX, y = offY}
end

function MapLayer:allocateRect()
    for y = 1, MapInfoConst.GRID_VER do
        for x = 1, MapInfoConst.GRID_HOR do
            if not self.rectList[x] then self.rectList[x] = {} end
            local rect = self:calculateRect(x, y)
            self.rectList[x][y] = rect
        end
    end
end

function MapLayer:calculateRect(x, y)
    local scaleX = MapInfoConst.X_INFO[y][x]["scaleX"]
    local scaleY = MapInfoConst.HOR_SCALE_Y[y]
    local _x = MapInfoConst.X_INFO[y][x]["x"]
    local _y = MapInfoConst.POS_Y[y]
    local width = GRID_HD_WIDTH * scaleX
    local height = GRID_HD_HEIGHT * scaleY
    return cc.rect(_x, _y, width, height)
end

function MapLayer:onTouchEvent(event)
    if event.name == "began" then
    elseif event.name == "moved" then
        self:onTouchMoved(event.x, event.y)
    elseif event.name == "ended" then
        self:onTouchEnded(event.x, event.y)
    end
end

function MapLayer:onTouchMoved()
end

function MapLayer:onTouchEnded(x, y)
    local gridIndex = self:getGridIndexByPoint(cc.p(x, y))
    if not gridIndex then return end
    EventMgr.trigEvent(EventType.TOUCH_ENDED_GRID, gridIndex)
end

function MapLayer:initMoveGrid()
    for _gridIndexY = 1, MapInfoConst.GRID_VER do
        for _gridIndexX = 1, MapInfoConst.GRID_HOR do
            local grid = Grid.new(_gridIndexX, _gridIndexY)
            grids_pool[_gridIndexX + _gridIndexY * MapInfoConst.GRID_HOR] = grid
        end
    end
end

function MapLayer:showMoveGrid(pathInfo)
    for _, info in pairs(pathInfo) do
        local grid = grids_pool[info.gridIndex.x + info.gridIndex.y * MapInfoConst.GRID_HOR]
        self:addChild(grid)
        table_insert(self.grid_visible, grid)
    end
end

function MapLayer:hideMoveGrid()
    if #self.grid_visible == 0 then return end

    for _, grid in ipairs(self.grid_visible) do
        grid:removeFromParent()
    end
end

function MapLayer:getStandPos(gridIndexX, gridIndexY)
    local rect = self.rectList[gridIndexX][gridIndexY]
    local x = rect.x + self.standOffset.x
    local y = rect.y + self.standOffset.y
    return x, y
end

function MapLayer:getStandPosToWorldSpace(gridIndexX, gridIndexY)
    local x, y = self:getStandPos(gridIndexX, gridIndexY)
    return self:convertToWorldSpace(cc.p(x, y))
end

function MapLayer:getGridIndexByPoint(point)
    for gridIndexX, tbl in ipairs(self.rectList) do
        for gridIndexY, rect in ipairs(tbl) do
            if cc.rectContainsPoint(rect, point) then
                return {x = gridIndexX, y = gridIndexY}
            end
        end
    end
end

return MapLayer
