local MapInfo = require("app.module.data.MapInfo") 
local MapInfoConst = require("app.const.MapInfoConst")
local TouchGrid = require("app.basicObj.TouchGrid")
local BaseLayer = require("app.module.layer.BaseLayer")
local TouchGridLayer = class("TouchGridLayer", BaseLayer)

local grid_pool = {}

function TouchGridLayer:ctor()
    TouchGridLayer.super.ctor(self)
    self:setTouchSwallowEnabled(false)
    self:initTouchGrid()
end

function TouchGridLayer:initTouchGrid()
    for _gridIndexY = 1, MapInfoConst.GRID_VER do
        for _gridIndexX = 1, MapInfoConst.GRID_HOR do
            local grid = TouchGrid.new(_gridIndexX, _gridIndexY)
            self:addChild(grid)
            grid_pool[_gridIndexX + _gridIndexY * MapInfoConst.GRID_HOR] = grid
        end
    end
end

function TouchGridLayer:getTouchGridByGridIndex(gridIndex)
    return grid_pool[gridIndex.x + gridIndex.y * MapInfoConst.GRID_HOR]
end

return TouchGridLayer
