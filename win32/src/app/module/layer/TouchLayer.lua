local BaseLayer = require("app.module.layer.BaseLayer")
local TouchLayer = class("TouchLayer", BaseLayer)
local EventMgr = require("app.manager.EventMgr")

function TouchLayer:ctor()
    TouchLayer.super.ctor(self)
    --self.mapLayer = InstanceMgr.getIns(InstanceMgr.MAPLAYER)
    --self.battleUILayer = InstanceMgr.getIns(InstanceMgr.BATTLEUILAYER)
    self.canTouch = true
    self:initEvent()
end

function TouchLayer:initEvent()
    self.listenner = cc.EventListenerTouchOneByOne:create()
    --self.listenner:setSwallowsTouches(false)

    self.listenner:registerScriptHandler(function(touch, event)
        if not self.canTouch then 
            print("TouchLayer can not touch!!!!!!!!!!!!!!!!!!!!")
            return
        end

        --print("TouchLayer touch began:", touch:getLocation().x, touch:getLocation().y)
        local touchInfo = {x = touch:getLocation().x, y = touch:getLocation().y, name = "began"}
        EventMgr.trigEvent(EventType.TOUCH_EVENT, touchInfo)
        return true
    end, cc.Handler.EVENT_TOUCH_BEGAN)

    self.listenner:registerScriptHandler(function(touch, event)
        if not self.canTouch then 
            print("TouchLayer can not touch!!!!!!!!!!!!!!!!!!!!")
            return
        end

        --print("TouchLayer touch moved:", touch:getLocation().x, touch:getLocation().y)
        local touchInfo = {x = touch:getLocation().x, y = touch:getLocation().y, name = "moved"}
        EventMgr.trigEvent(EventType.TOUCH_EVENT, touchInfo)
    end, cc.Handler.EVENT_TOUCH_MOVED)

    self.listenner:registerScriptHandler(function(touch, event)
        if not self.canTouch then 
            print("TouchLayer can not touch!!!!!!!!!!!!!!!!!!!!")
            return
        end

        --print("TouchLayer touch ended:", touch:getLocation().x, touch:getLocation().y)
        local touchInfo = {x = touch:getLocation().x, y = touch:getLocation().y, name = "ended"}
        EventMgr.trigEvent(EventType.TOUCH_EVENT, touchInfo)
    end, cc.Handler.EVENT_TOUCH_ENDED)

    self:getEventDispatcher():addEventListenerWithSceneGraphPriority(self.listenner, self) 

    --[[
    local function onTouchEvent(event)
        if event.name == "began" then
            print("TouchLayer touch began:", event.x, event.y)
            EventMgr.trigEvent(EventType.TOUCH_EVENT, touch)
            return true
        elseif event.name == "moved" then
            print("TouchLayer touch moved:", event.x, event.y)
        elseif event.name == "ended" then
            print("TouchLayer touch ended:", event.x, event.y)
        end

        if not self.canTouch then 
            print("TouchLayer can not touch!!!!!!!!!!!!!!!!!!!!")
            return
        end
        EventMgr.trigEvent(EventType.TOUCH_EVENT, touch)
    end
    ]]--
    --self:addNodeEventListener(cc.NODE_TOUCH_EVENT, onTouchEvent)
    --self:setTouchEnabled(true)
end

-- 加这个条件为了以后方便定位问题和调试
function TouchLayer:setTouch(bool)
    self.canTouch = bool
end

return TouchLayer
