local BaseLayer = require("app.module.layer.BaseLayer")
local BattleEffectLayer = class("BattleEffectLayer", BaseLayer)

function BattleEffectLayer:ctor()
    BattleEffectLayer.super.ctor(self)
end

return BattleEffectLayer
