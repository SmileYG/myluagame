local MapInfoConst = require("app.const.MapInfoConst")
local BasicObjConst = require("app.const.BasicObjConst")
local BattleUtil = require("app.util.BattleUtil")
local BaseLayer = require("app.module.layer.BaseLayer")
local BattleData = require("app.module.data.BattleData")
local CharLayer = class("CharLayer", BaseLayer)

function CharLayer:ctor()
    CharLayer.super.ctor(self)
    self.charsTbl = {}
    --self.charNum = 0
end

function CharLayer:addChar(char)
    --local gridIndex = char:getGridIndex()
    local id = char:getId()
    local gridIndex = BattleData.getGridIndexById(id)
    self.charsTbl[id] = char
    --self.charsTbl[gridIndex.x + gridIndex.y * MapInfoConst.GRID_HOR] = char
    --self.charNum = self.charNum + 1
    --char:setId(self.charNum)
    self:addChild(char)
    self:setCharPosition(char)
end

function CharLayer:setCharPosition(char)
    local gridIndex = BattleData.getGridIndexById(char:getId())
    local desPosition = BattleUtil.getDestinationPosition(gridIndex, char)
    char:setPosition(desPosition)
end

--[[
function CharLayer:getCharByGridIndex(gridIndex)
    return self.charsTbl[gridIndex.x + gridIndex.y * MapInfoConst.GRID_HOR]
end

function CharLayer:updateGridIndex(origGridIndex, desGridIndex)
    local n = origGridIndex.x + origGridIndex.y * MapInfoConst.GRID_HOR
    local char = self.charsTbl[n]
    self.charsTbl[desGridIndex.x + desGridIndex.y * MapInfoConst.GRID_HOR] = char
    self.charsTbl[n] = nil
end
]]--

function CharLayer:getCharById(id)
    if not self.charsTbl then return nil end

    return self.charsTbl[id]
    --[[
    for _, char in pairs(self.charsTbl) do
        if char:getId() == id then
            return char
        end
    end
    return nil
    ]]--
end

function CharLayer:getAllChars()
    return self.charsTbl
end

return CharLayer
