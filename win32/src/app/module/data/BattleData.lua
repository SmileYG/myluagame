local BattleData = {}

BattleData.tmpData = {
    [1] = {
        id = 1,
        heroId = 10001,
        hp = 300,
        cp = 0,
        pos = {1, 1},
        force = 20,
        buffersInfo = 
        {
            --{bufferName = "xxxx", rounds = 2},
        },
    },
    [2] = {
        id = 2,
        heroId = 10002,
        hp = 300,
        cp = 0,
        pos = {2, 3},
        force = 20,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [3] = {
        id = 3,
        heroId = 10003,
        hp = 300,
        cp = 0,
        pos = {3, 2},
        force = 20,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [4] = {
        id = 4,
        heroId = 10005,
        hp = 300,
        cp = 0,
        pos = {2, 4},
        force = 20,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [5] = {
        id = 5,
        heroId = 10006,
        hp = 300,
        cp = 0,
        pos = {1, 5},
        force = 20,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [6] = {
        id = 6,
        heroId = 10007,
        hp = 300,
        cp = 0,
        pos = {9, 1},
        force = 10,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [7] = {
        id = 7,
        heroId = 10008,
        hp = 300,
        cp = 0,
        pos = {8, 2},
        force = 10,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [8] = {
        id = 8,
        heroId = 10013,
        hp = 300,
        cp = 0,
        pos = {7, 3},
        force = 10,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [9] = {
        id = 9,
        heroId = 10010,
        hp = 300,
        cp = 0,
        pos = {8, 4},
        force = 10,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
    [10] = {
        id = 10,
        heroId = 10011,
        hp = 300,
        cp = 0,
        pos = {9, 5},
        force = 10,
        buffersInfo = 
        {
            --id = "xxxx",
            --SurValue = 2,
        },
    },
}

-------------------------item--------------------------

BattleData.itemData = {
    [1] = "item_1",
    [2] = "item_2",
    [3] = "item_3",
    [4] = "item_4",
    [5] = "item_5",
}

-------------------------------------------------------


function BattleData.initData()
    BattleData.charData = {}
    for _, info in ipairs(BattleData.tmpData) do
        local tbl = {}
        tbl.x = info.pos[1]
        tbl.y = info.pos[2]
        info.pos = tbl
        info.buffersIdList = {}
        BattleData.charData[info.id] = info
    end
    BattleData.charBakData = clone(BattleData.charData)
end
BattleData.initData()

function BattleData.getGridIndexById(id)
    return BattleData.charData[id].pos
end

function BattleData.setGridIndexById(id, GridIndex)
    BattleData.charData[id].pos = GridIndex
end

function BattleData.getInitPosById(id)
    return BattleData.charBakData[id].pos
end

function BattleData.getHeroIdById(id)
    return BattleData.charData[id].heroId
end

function BattleData.getForceById(id)
    return BattleData.charData[id].force
end

-- hp
function BattleData.getHpById(id)
    return BattleData.charData[id].hp
end

function BattleData.setHpById(id, value)
    BattleData.charData[id].hp = value
end

function BattleData.updateHpById(id, value)
    BattleData.charData[id].hp = math.max(0, BattleData.charData[id].hp + value)
end

-- cp
function BattleData.getCpById(id)
    return BattleData.charData[id].cp
end

function BattleData.setCpById(id, value)
    BattleData.charData[id].cp = value
end

function BattleData.updateCpById(id, value)
    BattleData.charData[id].cp = math.max(0, BattleData.charData[id].cp + value)
end

function BattleData.getBuffersIdListById(id)
    return BattleData.charData[id].buffersIdList
end

function BattleData.setBufferIdById(id, bufferId)
    table.insert(BattleData.charData[id].buffersIdList, bufferId)
end

function BattleData.removeBufferById(id, bufferId)
    local list = BattleData.charData[id].buffersIdList, bufferId
    for i, _bufferId in pairs(list) do
        if bufferId == _bufferId then
            table.remove(list, i)
            return
        end
    end
end

function BattleData.getItemData()
    return BattleData.itemData
end

function BattleData.getCharData()
    return BattleData.charData
end

return BattleData
