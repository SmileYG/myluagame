local MapInfoConst = require("app.const.MapInfoConst")
local HeroInfo = require("app.config.HeroInfo")
--local SelectRange = require("app.config.SelectRange")
local AttackRange = require("app.config.AttackRange")
local RangeInfo = require("app.config.RangeInfo")
local CharConst = require("app.const.CharConst")
local BattleConst = require("app.const.BattleConst")
local BattleUtil = require("app.util.BattleUtil")
local BattleData = require("app.module.data.BattleData")

local MapInfo = class("MapInfo")

--local DIFF_HOR_WIDTH = (882 - 864)/2
--local VER_SCALE_X = {0.98, 0.97, 0.96, 0.94, 0.92}
--local VER_SCALE_X = {0.92, 0.94, 0.96, 0.97, 0.98}
--local HOR_SCALE_Y = {0.97, 0.89, 0.86, 0.84, 0.75}


-- 882/9 = 98, 864/9 = 96, 846/9 = 94 .......
--local HOR_WIDTH = {98, 96, 94, 92, 90}
--local HOR_WIDTH = {102, 101, 100, 99, 98}


local ipairs = ipairs
local pairs = pairs
local math_floor = math.floor
local math_abs = math.abs
local table_insert = table.insert

local dirs4 = {
    {x = -1, y = 0}, 
    {x = 0, y = 1}, 
    {x = 1, y = 0}, 
    {x = 0, y = -1},
}

local dirs8 = {
    {x = -1, y = 0}, 
    {x = 0, y = 1}, 
    {x = 1, y = 0}, 
    {x = 0, y = -1},
    {x = 1, y = -1},
    {x = -1, y = -1},
    {x = -1, y = 1},
    {x = 1, y = 1},
}
--local GRID_HD_WIDTH = 106
--local GRID_HD_HEIGHT = 105

function MapInfo:ctor()
    self:initGridAry()
end

-- 0:可行走区域，1:该位置有char
-- 2:xxxxxxx
function MapInfo:initGridAry()
    self.gridAry = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
    }
    self.enemysGridIndex = {}
    self.filteredTargetsGridIndex = {}
    self.ownsGridIndex = {}
    self.skillEnemysGridIndex = {}
    self.skillOwnsGridIndex = {}
    --[[
    self.rectList = {}
    self:allocateRect()

    -- 站位点偏移量
    local offX = math_floor(GRID_HD_WIDTH / 2)
    local offY = math_floor(GRID_HD_HEIGHT / 2 * 0.5)
    self.standOffset = {x = offX, y = offY}
    ]]--
end

--[[
function MapInfo:allocateRect()
    for y = 1, MapInfoConst.GRID_VER do
        for x = 1, MapInfoConst.GRID_HOR do
            if not self.rectList[x] then self.rectList[x] = {} end
            local rect = self:calculateRect(x, y)
            self.rectList[x][y] = rect
        end
    end
end

function MapInfo:calculateRect(x, y)
    local scaleX = MapInfoConst.X_INFO[y][x]["scaleX"]
    local scaleY = MapInfoConst.HOR_SCALE_Y[y]
    local _x = MapInfoConst.X_INFO[y][x]["x"]
    local _y = MapInfoConst.POS_Y[y]
    local width = GRID_HD_WIDTH * scaleX
    local height = GRID_HD_HEIGHT * scaleY
    return cc.rect(_x, _y, width, height)
end
]]--

function MapInfo:updateGridAry(_type, gridIndex, value)
end

-- 寻找可走路径
function MapInfo:searchMovePath(heroId, origin_gridIndex)
    self.pathInfo = {}

    local moveValue = HeroInfo[heroId]["MOV"]
    local origin_pathInfo = {}
    origin_pathInfo[origin_gridIndex.x + origin_gridIndex.y * MapInfoConst.GRID_HOR] = {
        ["gridIndex"] = origin_gridIndex,
        ["canMoveValue"] = moveValue,
    }
    self:_searchMovePath(origin_pathInfo, origin_gridIndex)
    return self.pathInfo
end

-- origin_pathInfo = {
--      pos = {x = xxx, y = xxx},
--      canMoveValue = xxx,
-- }
function MapInfo:_searchMovePath(origin_pathInfo, origin_gridIndex)
    local tmp_pathInfo = {}
    for _, _pathInfo in pairs(origin_pathInfo) do

        if _pathInfo.canMoveValue > 0 then
            for _, dir in ipairs(dirs4) do

                local _x = _pathInfo.gridIndex.x + dir.x
                local _y = _pathInfo.gridIndex.y + dir. y
                if _x ~= origin_gridIndex.x or _y ~= origin_gridIndex.y then
                    local next_gridIndex = {x = _x, y = _y}
                    if next_gridIndex.x >= 1 and next_gridIndex.y >= 1 and next_gridIndex.x <= MapInfoConst.GRID_HOR and next_gridIndex.y <= MapInfoConst.GRID_VER then 
                        if _pathInfo.canMoveValue > 0 then
                            local surMoveValue = _pathInfo.canMoveValue - 1
                            local index = next_gridIndex.x + next_gridIndex.y * MapInfoConst.GRID_HOR
                            if self:checkIsValid(next_gridIndex) then
                                if not self.pathInfo[index] or self.pathInfo[index].canMoveValue < surMoveValue then
                                    local info = {gridIndex = next_gridIndex, canMoveValue = surMoveValue}
                                    self.pathInfo[index] = info
                                    tmp_pathInfo[index] = info
                                end
                            else
                                local info = {gridIndex = next_gridIndex, canMoveValue = surMoveValue}
                                tmp_pathInfo[index] = info
                            end
                        end

                        -- 另一种寻路方式
                        --[[
                        if self:checkIsValid(next_gridIndex) then

                            --local surMoveValue = _pathInfo.canMoveValue - 1
                            if _pathInfo.canMoveValue > 0 then
                                local surMoveValue = _pathInfo.canMoveValue - 1

                                local index = next_gridIndex.x + next_gridIndex.y * MapInfoConst.GRID_HOR
                                if not self.pathInfo[index] or self.pathInfo[index].canMoveValue < surMoveValue then
                                    local info = {gridIndex = next_gridIndex, canMoveValue = surMoveValue}
                                    self.pathInfo[index] = info
                                    tmp_pathInfo[index] = info
                                end

                            end

                        end
                        ]]--

                    end

                end

            end

        end
        self:_searchMovePath(tmp_pathInfo, _pathInfo.gridIndex)
    end
end

-- 寻找目标
-- 返回范围内所有敌人、过滤掉的、己方的位置
function MapInfo:searchTargetsGridIndex(heroId, origGridIndex, attackMode)
    self.enemysGridIndex = {}
    self.filteredTargetsGridIndex = {}
    self.ownsGridIndex = {}
    local selectRange = RangeInfo[HeroInfo[heroId]["selectRange"]]
    
    --if attackMode == CharConst.ATTACK_MODE_NORMAL then
        --selectRange = RangeInfo[AttackRange[HeroInfo[heroId]["n_range"]]["range"]]
    --else
        --selectRange = RangeInfo[AttackRange[HeroInfo[heroId]["s_range"]]["range"]]
    --end

    for _, gridIndex in ipairs (selectRange) do
        local _x = origGridIndex.x + gridIndex.x
        local _y = origGridIndex.y + gridIndex.y
        if _x >= 1 and _y >= 1 and _x <= MapInfoConst.GRID_HOR and _y <= MapInfoConst.GRID_VER then 
            local _gridIndex = {x = _x, y = _y}
            local areaType = self:getAreaTypeByGridIndex(_gridIndex)
            if areaType == MapInfoConst.FORCE_ENEMY then
                self.enemysGridIndex[_gridIndex.x + _gridIndex.y * MapInfoConst.GRID_HOR] = _gridIndex
            elseif areaType == MapInfoConst.FORCE_OWN then
                self.ownsGridIndex[_gridIndex.x + _gridIndex.y * MapInfoConst.GRID_HOR] = _gridIndex
            end
        end
    end

    --if attackMode == CharConst.ATTACK_MODE_NORMAL then
        local attackType = HeroInfo[heroId]["attackType"]
        if attackType == BattleConst.ATTACK_TYPE_BLOCKED then
            --local attackDistance = HeroInfo[heroId]["attackDistance"]
            --local attackDistance = BattleUtil.calculateAttackDistance(selectRange)
            self:filterAttackTargetsGridIndex(origGridIndex, self.enemysGridIndex, self.filteredTargetsGridIndex)
        end
    --elseif attackMode == CharConst.ATTACK_MODE_SKILL then
        --local range = RangeInfo[AttackRange[HeroInfo[heroId]["s_range"]]["range"]]
        --self:filterSkillTargetsGridIndex(origGridIndex, range)
    --end

    return self.enemysGridIndex, self.filteredTargetsGridIndex, self.ownsGridIndex
end

-- 过滤目标，保留不被阻挡的攻击目标
function MapInfo:filterAttackTargetsGridIndex(origGridIndex, targetsGridIndex, filteredTargetsGridIndex)
    for index, gridIndex in pairs(targetsGridIndex) do
        self:_filterAttackTargetsGridIndex(origGridIndex, gridIndex, filteredTargetsGridIndex)
    end
end

function MapInfo:_filterAttackTargetsGridIndex(origGridIndex, goalGridIndex, filteredTargetsGridIndex)
    if not self:isEqual(origGridIndex, goalGridIndex) then
        local _x = origGridIndex.x - goalGridIndex.x
        local _y = origGridIndex.y - goalGridIndex.y
        if _x == 0 or _y == 0 or math_abs(_x) == math_abs(_y) then
            self:checkArea(goalGridIndex, _x, _y)
        end
    end
end

function MapInfo:searchSkillTargetsGridIndex(origGridIndex, heroId)
    local range = RangeInfo[AttackRange[HeroInfo[heroId]["s_range"]]["range"]]
    self.skillEnemysGridIndex = {}
    self.skillOwnsGridIndex = {}
    for _, gridIndex in ipairs(range) do
        local _x = origGridIndex.x + gridIndex.x
        local _y = origGridIndex.y + gridIndex.y
        local key = _x + _y * MapInfoConst.GRID_HOR
        local _gridIndex = {x = _x, y = _y}
        local areaType = self:getAreaTypeByGridIndex(_gridIndex)
        if areaType == MapInfoConst.FORCE_ENEMY then
            self.skillEnemysGridIndex[key] = _gridIndex
        elseif self.ownsGridIndex[key] then
            self.skillOwnsGridIndex[key] = _gridIndex
        end
    end
    return self.skillEnemysGridIndex, self.skillOwnsGridIndex
end

function MapInfo:isEqual(origGridIndex, goalGridIndex)
    for _, dir in ipairs(dirs8) do
        local _x = origGridIndex.x + dir.x
        local _y = origGridIndex.y + dir.y
        if _x == goalGridIndex.x and _y == goalGridIndex.y then
            return true
        end
    end
    return false
end

function MapInfo:checkArea(gridIndex, offsetX, offsetY)
    local n = 0
    local dir = {}
    local a = math_abs(offsetX)
    local b = math_abs(offsetY)

    if offsetX == 0 then
        dir.x = 0
    else
        dir.x = offsetX / a
    end

    if offsetY == 0 then
        dir.y = 0
    else
        dir.y = offsetY / b
    end
    
    if a > 0 then
        n = a 
    elseif b > 0 then
        n = b
    end

    for i = 1, n do
        local tmp = {x = gridIndex.x + dir.x * i, y = gridIndex.y + dir.y * i}
        local areaType = self:getAreaTypeByGridIndex(tmp)
        if areaType == MapInfoConst.FORCE_ENEMY then
            self.filteredTargetsGridIndex[gridIndex.x + gridIndex.y * MapInfoConst.GRID_HOR] = gridIndex
        end
    end
end

--[[
function MapInfo:getGridIndexByPoint(point)
    for gridIndexX, tbl in ipairs(self.rectList) do
        for gridIndexY, rect in ipairs(tbl) do
            if cc.rectContainsPoint(rect, point) then
                return {x = gridIndexX, y = gridIndexY}
            end
        end
    end
end
]]--

function MapInfo:searchownsGridIndex(heroId, gridIndex)
    local targetsGridIndex = {}
end

-- 外部调用
function MapInfo:canMoveByGridIndex(gridIndex)
    for _, info in pairs(self.pathInfo) do
        if info.gridIndex.x == gridIndex.x and info.gridIndex.y == gridIndex.y then
            return true
        end
    end
    return false
end

function MapInfo:canAttackByGridIndex(gridIndex)
    for _, _gridIndex in pairs(self.filteredTargetsGridIndex) do
        if _gridIndex.x == gridIndex.x and _gridIndex.y == gridIndex.y then
            return false
        end
    end

    for _, _gridIndex in pairs(self.enemysGridIndex) do
        if _gridIndex.x == gridIndex.x and _gridIndex.y == gridIndex.y then
            return true
        end
    end
    return false
end

-- 根据攻击范围找范围内的攻击目标
function MapInfo:findTargets(rangeId, attackerGridIndex, targetGridIndex)
    local tmpTbl = {}
    local range = RangeInfo[rangeId]
    local gridIndex = attackerGridIndex

    if #range == 1 and range[1].x == 0 and range[1].y == 0 then
       gridIndex = targetGridIndex
    end

    for _, offset in ipairs(range) do
        local _x = gridIndex.x + offset.x
        local _y = gridIndex.y + offset.y
        if _x >= 1 and _y >= 1 and _x <= MapInfoConst.GRID_HOR and _y <= MapInfoConst.GRID_VER then 
            local _gridIndex = {x = _x, y = _y}
            local areaType = self:getAreaTypeByGridIndex(_gridIndex)
            if areaType == MapInfoConst.FORCE_ENEMY then
                table_insert(tmpTbl, _gridIndex)
            end
        end
    end
    return tmpTbl
end
--

-- 获取item适合使用的所有位置
function MapInfo:getItemApplyRange(targetType)
    local tmpTbl = {}
    for y, ary in ipairs(self.gridAry) do
        for x, areaType in ipairs(ary) do
            if targetType == 0 then
                local info = {gridIndex = {x = x, y = y}}
                table_insert(tmpTbl, info)
            else
                local dValue = targetType - areaType
                if dValue >= 0 and dValue <= 10 then
                    local info = {gridIndex = {x = x, y = y}}
                    table_insert(tmpTbl, info)
                end
            end
        end
    end
    return tmpTbl
end

function MapInfo:getCharIdFromGrid(gridIndex)
    return self.gridAry[gridIndex.y][gridIndex.x]
end

function MapInfo:setCharIdToGrid(id, origGridIndex, desGridIndex)
    if origGridIndex then
        self.gridAry[origGridIndex.y][origGridIndex.x] = 0
    end
    self.gridAry[desGridIndex.y][desGridIndex.x] = id
end

function MapInfo:getAreaTypeByGridIndex(gridIndex)
    local id = self:getCharIdFromGrid(gridIndex)
    local _type
    if id == 0 then
        _type = MapInfoConst.FORCE_NULL
    else
        _type = BattleData.getForceById(id)
    end

    return _type
end

--[[
function MapInfo:setItemGrid(gridIndex)
end
]]--

function MapInfo:checkIsValid(gridIndex)
    if self.gridAry[gridIndex.y][gridIndex.x] <= 0 then
        return true
    end
    return false
end

function MapInfo:getEnemysGridIndex()
    return self.enemysGridIndex
end

function MapInfo:getOwnsGridIndex()
    return self.ownsGridIndex
end

function MapInfo:getFilteredTargetsGridIndex()
    return self.filteredTargetsGridIndex
end

function MapInfo:getSkillEnemysGridIndex()
    return self.skillEnemysGridIndex
end

function MapInfo:resetSkillEnemysGridIndex()
    self.skillEnemysGridIndex = {}
end

function MapInfo:getSkillOwnsGridIndex()
    return self.skillOwnsGridIndex
end

--[[
function MapInfo:getStandPos(gridIndexX, gridIndexY)
    local rect = self.rectList[gridIndexX][gridIndexY]
    local x = rect.x + self.standOffset.x
    local y = rect.y + self.standOffset.y
    return x, y
end
]]--

--[[
function MapInfo:getStandPosToWorldSpace(gridIndexX, gridIndexY)
    local x, y = self:getStandPos(gridIndexX, gridIndexY)
    return self:convertToWorldSpace(cc.p(x, y))
end
]]--

function MapInfo:getGridAry()
    return self.gridAry
end

return MapInfo
