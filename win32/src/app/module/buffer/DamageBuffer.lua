local Buffer = require("app.module.buffer.Buffer")
local DamageBuffer = class("DamageBuffer", Buffer)
local BattleUtil = require("app.util.BattleUtil")
local CharConst = require("app.const.CharConst")

function DamageBuffer:ctor(info)
    DamageBuffer.super.ctor(self, info)
end

function DamageBuffer:getFinalValue()
    local value = self:getValue()
    if not value then
        value = self:calFinalValue()
    end

    return -value
end

function DamageBuffer:calFinalValue()
    -- 施加者
    local fa1 = BattleUtil.getCharFA(self.reId, CharConst.PROP_STR)
    -- 被施加者
    local fa2 = BattleUtil.getCharFA(self.targetId, CharConst.PROP_DEF)
    return math.max(fa1 - fa2, 0)
end

--[[
function DamageBuffer:getFinalValue()
    local value
    if not self:isPercent() then
        value = self:getValue()
        if not value then
            -- 需要根据攻击方和被攻击方进行计算得出
            value = self:calFinalValue()
        end
    else
        -- 百分比伤害： 血量*百分比 
        local fa = BattleUtil.getCharBA(self.targetId, CharConst.PROP_HP)
        value = fa * self:getValue()
    end

    return -1 * value
end

-- 攻击者的总伤害 - 被攻击者的总防御（包括攻击加成，防御加成，护盾，免伤等等）
function DamageBuffer:calFinalValue()
    local fa1, fa2, ba
    ba = BattleUtil.getCharBA(self.reId, CharConst.PROP_STR)
    -- 判断是物理攻击还是魔法攻击
    if ba then
        -- 施加者
        fa1 = BattleUtil.getCharFA(self.reId, CharConst.PROP_STR)
        -- 被施加者
        fa2 = BattleUtil.getCharFA(self.targetId, CharConst.PROP_DEF)
    else
        -- 施加者
        fa1 = BattleUtil.getCharFA(self.reId, CharConst.PROP_ATS)
        -- 被施加者
        fa2 = BattleUtil.getCharFA(self.targetId, CharConst.PROP_ADF)
    end
    print("calHpFinalValue:",fa1,fa2)
    return math.max(fa1 * fa2 / 100, 0)
end
]]--

return DamageBuffer
