local BufferInfo = require("app.config.BufferInfo")
local Buffer = class("Buffer")
--local EventMgr = require("app.manager.EventMgr")

function Buffer:ctor(info)
    self.info = info
    self.id = info.id

    --self:checkValue(_info.v)
end

function Buffer:work()
end

--[[
function Buffer:checkValue(v)
    self._isPercent = toBoolean(string.find(v, "%"))
    local b = string.find(v, "b")
    local f = string.find(v, "f")

    if b then
        if self._isPercent then
            self:setBasicValue(tonumber(string.sub(v, 1, b - 2)) / 100)
        else
            self:setBasicValue(tonumber(string.sub(v, 1, b - 1)))
        end
    elseif f then
        if self._isPercent then
            self:setFinalValue(tonumber(string.sub(v, 1, b - 2)) / 100)
        else
            self:setFinalValue(tonumber(string.sub(v, 1, b - 1)))
        end
    end
end
]]--

function Buffer:setReId(charId)
    self.reId = charId
end

function Buffer:setTargetId(charId)
    self.targetId = charId
end

function Buffer:setRounds(value)
    self.info.rounds = value
end

--[[
function Buffer:setBasicValue(value)
    self.info.bv = value
end

function Buffer:setFinalValue(value)
    self.info.fv = value
end
]]--

function Buffer:setValue(value)
    self.info.value = value
end

function Buffer:getReId()
    return self.reId
end

function Buffer:getTargetId()
    return self.targetId
end

--[[
function Buffer:getBasicValue()
    return self.info.bv
end

function Buffer:getFinalValue()
    return self.info.fv
end
]]--
function Buffer:getFinalValue()
    return self:getValue()
end

function Buffer:getValue()
    return self.info.value
end

function Buffer:updateRounds(v)
    self.info.rounds = math.max(0, self.info.rounds + v)
end

function Buffer:setRounds(v)
    self.info.rounds = v
end

-- buffer持续回合数
function Buffer:getRounds()
    return self.info.rounds
end

function Buffer:getTriggerEffect()
    return self.info.effectTrigger
end

--[[
-- buffer是否产生作用
function Buffer:isAtOnce()
end
]]--

function Buffer:getEffectId()
    return self.info.effectId
end

-- buffer属性类型
function Buffer:getPropType()
    return self.info.propType
end

function Buffer:getBufferType()
    return self.info.bufferType
end

-- buffer值是否百分比
function Buffer:isPercent()
    return toBooleanEx(self.info.isPercent)
end

function Buffer:getMountPointName()
    return self.info.mountPointName
end

function Buffer:getId()
    return self.id
end

return Buffer
