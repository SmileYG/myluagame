local Buffer = require("app.module.buffer.Buffer")
local HpBuffer = class("HpBuffer", Buffer)
local CharConst = require("app.const.CharConst")

function HpBuffer:ctor(info)
    HpBuffer.super.ctor(self, info)
end

function HpBuffer:getFinalValue()
    local value
    if not self:isPercent() then
        value = self:getValue()
    else
        -- 百分比回血： 血量*百分比 
        local fa = BattleUtil.getCharBA(self.targetId, CharConst.PROP_HP)
        value = fa * self:getValue()
    end

    return value
end

return HpBuffer

