local ARROWS_RES = "res/ui/battleUI/arrows.png"

local AttackTipsArrows = class("AttackTipsArrows", function()
    return display.newSprite(ARROWS_RES)
end)
local BattleUtil = require("app.util.BattleUtil")

function AttackTipsArrows:ctor(pos)
    self.pos = pos
    self.battleUILayer = InstanceMgr.getIns(InstanceMgr.BATTLEUILAYER)
    self:init()
end

function AttackTipsArrows:init()
    self.size = self:getContentSize()
    self:setScaleY(0.3)
    self:setScaleX(0.001)
    self:setAnchorPoint(0, 0.5)
    self:setLocalZOrder(100)
    self:setPosition(self.pos)
    self.battleUILayer:addChild(self)
end

function AttackTipsArrows:setForm(angle, toPoint)
    self:setRotation(angle)
    local dis = BattleUtil.getDistanceByPoint(self.pos, toPoint)
    local rate = dis / self.size.width
    self:setScaleX(rate)
end

function AttackTipsArrows:destory()
    self:removeFromParent()
end

return AttackTipsArrows
