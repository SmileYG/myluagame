local TestScene = class("TestScene", function()
    return display.newScene("TestScene")
end)

function TestScene:ctor()
    print("TestScene ctor.................")
end

function TestScene:onEnter()
    print("TestScene onEnter..................")
end

function TestScene:onExit()
    print("TestScene onExit..................")
end

return TestScene
