local Char = require("app.basicObj.Char")
local MapLayer = require("app.module.layer.MapLayer")
local CharLayer = require("app.module.layer.CharLayer")
local BattleUILayer = require("app.module.layer.BattleUILayer")
local BattleEffectLayer = require("app.module.layer.BattleEffectLayer")
--local TouchGridLayer = require("app.module.layer.TouchGridLayer")
local TouchLayer = require("app.module.layer.TouchLayer")
local MapInfo = require("app.module.data.MapInfo")
local MapInfoConst = require("app.const.MapInfoConst")
local BattleMgr = require("app.manager.BattleMgr")
local SkillMgr = require("app.manager.SkillMgr")
local BufferMgr = require("app.manager.BufferMgr")
local EffectMgr = require("app.manager.EffectMgr")
local BasicObjConst = require("app.const.BasicObjConst")
local BattleData = require("app.module.data.BattleData")
local EventMgr = require("app.manager.EventMgr")
local SoundMgr = require("app.manager.SoundMgr")
--
local BattleScene = class("BattleScene", cc.load("mvc").ViewBase)

--[[
local BattleScene = class("BattleScene", function()
    return display.newScene("BattleScene")
end)
]]--

-- touchGridLayer zOrder is the highest
local TOUCH_LAYER_ZORDER = 100
local BATTLEUI_LAYER_ZORDER = 90

function BattleScene:ctor(app, name)
    BattleScene.super.ctor(self, app, name)
end

function BattleScene:onEnter()
    --ccexp.AudioEngine:preload("res/sound/Amb_Field_LP.mp3")
    self:initScene()
    self:initEvent()
    self.soundMgr:playMusic(1)
    --[[
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("res/skill_ani_1003_10.png", "res/skill_ani_1003_10.plist","res/skill_ani_1003_1.xml")
    local n = display.newNode()
    local a = ccs.Armature:create("skill_ani_1003_1")
    a:getAnimation():play("hit")
    --n:addChild(a)
    a:setPosition(cc.p(200, 200))
    self:addChild(a)
    ]]--
end

function BattleScene:initEvent()
    self.func = handler(self, self.onResetScene)
    EventMgr.regEvent(EventType.RESET_SCENE, self.func)
end

function BattleScene:unRegEvent()
    EventMgr.unRegEvent(EventType.RESET_SCENE, self.func)
end

function BattleScene:initScene()
    self:initBg()
    self:onResetScene()
end

function BattleScene:initBg()
    local sp = display.newSprite("bg/GameBoard03-hd.png")
    --local sp = display.newSprite("bg/heroGameBg.png")
    sp:setPosition(display.cx, display.cy)
    self:addChild(sp)
end

function BattleScene:initBaseModule()
    self.mapInfo = MapInfo.new()
    InstanceMgr.setIns(InstanceMgr.MAPINFO, self.mapInfo)

    local mapLayer = MapLayer.new()
    self:addChild(mapLayer)
    InstanceMgr.setIns(InstanceMgr.MAPLAYER, mapLayer)

    local charLayer = CharLayer.new()
    self:addChild(charLayer)
    InstanceMgr.setIns(InstanceMgr.CHARLAYER, charLayer)

    local battleUILayer = BattleUILayer.new()
    self:addChild(battleUILayer)
    InstanceMgr.setIns(InstanceMgr.BATTLEUILAYER, battleUILayer)

    local battleEffectLayer = BattleEffectLayer.new()
    self:addChild(battleEffectLayer)
    InstanceMgr.setIns(InstanceMgr.BATTLEEFFECTLAYER, battleEffectLayer)

    local touchLayer = TouchLayer.new()
    self:addChild(touchLayer, TOUCH_LAYER_ZORDER)
    InstanceMgr.setIns(InstanceMgr.TOUCHLAYER, touchLayer)

    local skillMgr = SkillMgr.new()
    InstanceMgr.setIns(InstanceMgr.SKILLMGR, skillMgr)

    local bufferMgr = BufferMgr.new()
    InstanceMgr.setIns(InstanceMgr.BUFFERMGR, bufferMgr)

    local effectMgr = EffectMgr.new()
    InstanceMgr.setIns(InstanceMgr.EFFECTMGR, effectMgr)

    self.soundMgr = SoundMgr.new()
    InstanceMgr.setIns(InstanceMgr.SOUNDMGR, self.soundMgr)

    -- battleMgr应最后初始化
    local battleMgr = BattleMgr.new()
    InstanceMgr.setIns(InstanceMgr.BATTLEMGR, battleMgr)
end

function BattleScene:initChar()
    local charData = BattleData.getCharData()
    local charLayer = InstanceMgr.getIns(InstanceMgr.CHARLAYER)
    for i, data in pairs(charData) do
        if data.id then
            local char = Char.new(data.heroId, data.id)
            --local grid = {x = data.pos[1], y = data.pos[2]}
            --char:setGridIndex(grid)
            if data.force == MapInfoConst.FORCE_OWN then
                char:setInitDir(BasicObjConst.DIR_LEFT)
            elseif data.force == MapInfoConst.FORCE_ENEMY then
                char:setInitDir(BasicObjConst.DIR_RIGHT)
            end
            --char:setForce(data.force)
            char:setCurHp(data.hp)
            char:setCurCp(data.cp)
            char:getAnimation():play("stand")
            charLayer:addChar(char)
            self.mapInfo:setCharIdToGrid(data.id, nil, data.pos)
        end
    end
end

function BattleScene:onResetScene()
    print("start initBaseModule......")
    self:initBaseModule()
    self:initChar()
end

function BattleScene:onExit()
    self:unRegEvent()
end

return BattleScene
