cc.FileUtils:getInstance():setPopupNotify(false)
cc.FileUtils:getInstance():addSearchPath("src/")
cc.FileUtils:getInstance():addSearchPath("res/")
cc.FileUtils:getInstance():addSearchPath("res/char/")
cc.FileUtils:getInstance():addSearchPath("res/bg/")
cc.FileUtils:getInstance():addSearchPath("res/ui/")
cc.FileUtils:getInstance():addSearchPath("res/effect/")

require("config")
require("cocos.init")
require("app.preload")
require("app.manager.InstanceMgr")

local function main()
    require("app.MyApp"):create():run()
end

local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
    print(msg)
end
